#ifndef SERIALHALHANDLER_H
#define SERIALHALHANDLER_H

#include <HALInterface.h>
#include <Windows.h>
#include <vector>
#include <ceddk.h>
#include <serhw.h>
#include <Serdbg.h>
#include <cserpdd.h>

#define MAXSERIALDEVICES 64			//Maximum of virtual serial ports we accept

class CSHelper
{
private:
	CRITICAL_SECTION* mCS;
public:
	CSHelper(CRITICAL_SECTION& cs) { 
		mCS=&cs;
		EnterCriticalSection(mCS);
	}
	~CSHelper() { LeaveCriticalSection(mCS);}
};

class CSerialEuphoriaHALPDD;

class CSerialDeviceHandler
{
public:
	static CSerialDeviceHandler* CreateDeviceHandler();
	HANDLE mHAL;

protected:
	CSerialDeviceHandler();
	~CSerialDeviceHandler();
	void addDevice(CSerialEuphoriaHALPDD* device);
	void removeDevice(CSerialEuphoriaHALPDD* device);
	bool initDeviceHandler();

private:
	std::vector<CSerialEuphoriaHALPDD*> mDevices;
	static DWORD deviceThreadStart(LPVOID lpParameter);
	DWORD deviceThread();

	HANDLE mGlobalDeviceThread;
	DWORD mGlobalDeviceThreadID;
	HANDLE mDeviceEvent;			//Event signaled when a device is initialized, or freed
	bool mGlobalDeviceThreadRunning;
	CRITICAL_SECTION mDeviceMutex;
	HANDLE mWaitEvents[MAXSERIALDEVICES+1];
	bool mDevicesChanged;
	bool mDeviceHandlerInitialized;

	friend class CSerialEuphoriaHALPDD;
};

#endif // SERIALHALHANDLER_H
