#include "SerialHALBase.h"


CSerialEuphoriaHALPDD::CSerialEuphoriaHALPDD(LPTSTR lpActivePath, PVOID pMdd, PHWOBJ pHwObj, int deviceArrayIndex )
	: CSerialPDD(lpActivePath,pMdd,pHwObj)
	, mMSQReadHandle(NULL)
	, mMSQWriteHandle(NULL)
	, mDeviceIndex(deviceArrayIndex)
	, mWriteEnabled(false)
	, mBackupBuf(NULL)
	, mBackupRead(0)
{
	DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD::CSerialEuphoriaHALPDD ( %d )\r\n"), deviceArrayIndex ) );
}

CSerialEuphoriaHALPDD::~CSerialEuphoriaHALPDD()
{
	if( mBackupBuf ) {
		free( mBackupBuf );
	}
	DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD::~CSerialEuphoriaHALPDD ( %d )\r\n"), mDeviceIndex ) );
}

/*
BOOL CSerialEuphoriaHALPDD::Init()
{
	//Check if we may create this device here. If no no need to continue!!
	if (DeviceIoControl(mHAL,IOCTL_EUPHORIAHAL_SERIAL_DEVICEEXIST,&mDeviceIndex,sizeof(DWORD),NULL,0,NULL,NULL)) {
		return CSerialPDD::Init();
	}
	return FALSE;
}
*/

void CSerialEuphoriaHALPDD::SerialRegisterBackup()
{
	//Do nothing for now
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::SerialRegisterBackup\r\n") ));
}

void CSerialEuphoriaHALPDD::SerialRegisterRestore()
{
	//Do nothing for now
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::SerialRegisterRestore\r\n") ));
}

//  Tx Function.
BOOL CSerialEuphoriaHALPDD::InitXmit(BOOL bInit)
{
	//We use this to actually start reading and writing from the port
	//Send message to open or close the real hardware
	EUHALSerial::EUHALSerialSET data;
	data.DeviceIndex=mDeviceIndex;
	data.value=bInit;
	return true;
//	return DeviceIoControl(mHAL,IOCTL_EUPHORIAHAL_SERIAL_INIT,&data,sizeof(EUHALSerialSET),NULL,0,NULL,NULL);
}

void CSerialEuphoriaHALPDD::XmitInterruptHandler(PUCHAR pTxBuffer, ULONG *pBuffLen)
{
	ULONG oBuffLen = *pBuffLen;
	if (*pBuffLen==0) {
		EnableXmitInterrupt(FALSE);
	} else {
	//Send all the bytes using MSMQ
		WriteMsgQueue(mMSQWriteHandle,pTxBuffer,*pBuffLen,INFINITE, 0);
		EnableXmitInterrupt(TRUE);
	}
}

void CSerialEuphoriaHALPDD::XmitComChar(UCHAR ComChar)
{
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::XmitComChar\r\n") ));
	//Send 1 byte using MSMQ
}

BOOL CSerialEuphoriaHALPDD::EnableXmitInterrupt(BOOL bEnable)
{
	mWriteEnabled = (bEnable == TRUE);
	//Enable the Xmit interrupt (As it is using MSMQ it's already enabled)
	return TRUE;
}

BOOL CSerialEuphoriaHALPDD::CancelXmit()
{
	return DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_CANCELXMIT,&mDeviceIndex,sizeof(DWORD),NULL,0,NULL,NULL);
}

//
//  Rx Function.
BOOL CSerialEuphoriaHALPDD::InitReceive(BOOL bInit)
{
	//Don't need stub function always returns true
	return TRUE;
}

ULONG CSerialEuphoriaHALPDD::ReceiveInterruptHandler(PUCHAR pRxBuffer,ULONG *pBuffLen)
{
	ULONG oBuffLen = *pBuffLen;
	if( mBackupBuf && mBackupRead ) {
		ULONG mm = min( oBuffLen, mBackupRead );
		DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD(%d)::ReceiveInterruptHandler fallback old bytes: %d\r\n"), mDeviceIndex, mm ) );
		memcpy( pRxBuffer, mBackupBuf, mm );
		*pBuffLen = mm;
		//pRxBuffer += mm;
		if( mm == mBackupRead ) {
			free( mBackupBuf );
			mBackupBuf = 0;
			mBackupRead = 0;
			DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD(%d)::ReceiveInterruptHandler fallback done\r\n"), mDeviceIndex, mm ) );
		} else {
			DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD(%d)::ReceiveInterruptHandler buffer full, remainder: %d\r\n"), mDeviceIndex, mBackupRead-mm ) );
			memmove( mBackupBuf, mBackupBuf + mm, mBackupRead - mm );
			mBackupRead -= mm;
		}
		return 0;
	}

	DWORD dwFlags;
	BOOL bRet = ReadMsgQueue( mMSQReadHandle, pRxBuffer, oBuffLen, pBuffLen, 0, &dwFlags );
	DWORD dwErr = 0;
	ULONG uRet = 0;
	if (!bRet) {
		dwErr = GetLastError();
		DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD(%d)::ReceiveInterruptHandler fail: %d\r\n"), mDeviceIndex, dwErr ) );
		if( 122 == dwErr ) {
			__declspec( align( 16 ) )char tmp[1024];
			ULONG dataLen = sizeof(tmp);

			bRet = ReadMsgQueue( mMSQReadHandle, tmp, dataLen, &dataLen, 0, &dwFlags );
			if( bRet ) {
				if( oBuffLen && dataLen ) {
					ULONG mm = min( oBuffLen, dataLen );
					memcpy( pRxBuffer, tmp, mm );
					*pBuffLen = mm;
					if( mm != dataLen ) {	//oBufLen was the smallest
						mBackupRead = dataLen - mm;
						if( mBackupBuf ) {
							free( mBackupBuf );
						}
						mBackupBuf = (char*)malloc( mBackupRead );
						memcpy( mBackupBuf, tmp + mm, mBackupRead );
					}
					DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD(%d)::ReceiveInterruptHandler fallback: stored %d for next call\r\n"), mDeviceIndex, mBackupRead ) );
					//SetEvent( mDeviceEvent );
				}
			}
		}
	}
	
	return uRet;
}

ULONG CSerialEuphoriaHALPDD::CancelReceive()
{
	DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_CANCELRECEIVE,&mDeviceIndex,sizeof(DWORD),NULL,0,NULL,NULL);
	return 0;
}

//
//  Modem

BOOL CSerialEuphoriaHALPDD::InitModem(BOOL bInit)
{
	//Called with true once port is initialized, and once while port is destroyed
	//A good place to initialize the real hardware here, send a message to initialize the hardware
	//	mj: destroy doesnt seem to get called...
	//		it's called in the destructor of the base class, but at this point the virtuals are back to the original ones..

	DEBUGMSG(ZONE_INIT,(TEXT("+CSerialEuphoriaHALPDD::InitModem( %d, init: %d )\r\n"), mDeviceIndex, bInit ));
	if (bInit) {
		if (DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_INIT,&mDeviceIndex,sizeof(DWORD),&mHALInitializationData,sizeof(EUHALSerial::EUHALSerialInit),NULL,NULL)) {
			MSGQUEUEOPTIONS msgQueueOptions;
			msgQueueOptions.dwSize=sizeof(MSGQUEUEOPTIONS);
			msgQueueOptions.dwFlags=MSGQUEUE_ALLOW_BROKEN | MSGQUEUE_NOPRECOMMIT;
			msgQueueOptions.dwMaxMessages=0;
			msgQueueOptions.cbMaxMessage=1024;
			msgQueueOptions.bReadAccess=TRUE;
			mMSQReadHandle=CreateMsgQueue( mHALInitializationData.MSQNameRead,&msgQueueOptions);
			DEBUGMSG( ZONE_INIT,( TEXT( "CSerialEuphoriaHALPDD (%d, %s) readHandle: %x\r\n"), mDeviceIndex, mHALInitializationData.MSQNameRead, mMSQReadHandle ) );
			if (mMSQReadHandle!=0) {
				msgQueueOptions.bReadAccess=FALSE;
				mMSQWriteHandle=CreateMsgQueue(mHALInitializationData.MSQNameWrite,&msgQueueOptions);
				if (mMSQWriteHandle!=0) {
					g_DeviceHandler->addDevice(this);
					return TRUE;
				}
			}
			ERRORMSG(TRUE,(TEXT("CSerialEuphoriaHALPDD::InitModem Error initializing port index:%d creating MSMQ failed\r\n"),mDeviceIndex));
		} else {
			ERRORMSG(TRUE,(TEXT("CSerialEuphoriaHALPDD::InitModem Error initializing port index:%d\r\n"),mDeviceIndex));
		}
	} else {
		g_DeviceHandler->removeDevice(this);
		DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_FREE,&mDeviceIndex,sizeof(DWORD),NULL,0,NULL,NULL);
		if (mMSQReadHandle) {
			CloseMsgQueue(mMSQReadHandle);
		}
		if (mMSQWriteHandle) {
			CloseMsgQueue(mMSQWriteHandle);
		}
		DEBUGMSG(ZONE_INIT,(TEXT("-CSerialEuphoriaHALPDD::InitModem Dev removed\r\n") ));
		return TRUE;
	}
	DEBUGMSG(ZONE_INIT,(TEXT("-CSerialEuphoriaHALPDD::InitModem Error\r\n") ));

	//Error initializing port!!
	return FALSE;
}

void CSerialEuphoriaHALPDD::ModemInterruptHandler()
{
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::ModemInterruptHandler\r\n") ));
}

ULONG CSerialEuphoriaHALPDD::GetModemStatus()
{
	//Get modem status using IOCTL
	DWORD modemStatus=0;
	DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_GETMODEMSTATUS,&mDeviceIndex,sizeof(DWORD),&modemStatus,sizeof(DWORD),NULL,NULL);
	return modemStatus;
}

void CSerialEuphoriaHALPDD::SetDTR(BOOL bSet)
{
	EUHALSerial::EUHALSerialSET data;
	data.DeviceIndex=mDeviceIndex;
	data.value=bSet;
	DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_SETDTR,&data,sizeof(EUHALSerial::EUHALSerialSET),NULL,0,NULL,NULL);
}

void CSerialEuphoriaHALPDD::SetRTS(BOOL bSet)
{
	EUHALSerial::EUHALSerialSET data;
	data.DeviceIndex=mDeviceIndex;
	data.value=bSet;
	DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_SETRTS,&data,sizeof(EUHALSerial::EUHALSerialSET),NULL,0,NULL,NULL);
}

//  Line Function
BOOL CSerialEuphoriaHALPDD::InitLine(BOOL bInit)
{
	//Don't need stub function always returns true
	return TRUE;
}

void CSerialEuphoriaHALPDD::LineInterruptHandler()
{
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::LineInterruptHandler\r\n") ));

	//Do nothing??? 
}

void CSerialEuphoriaHALPDD::SetBreak(BOOL bSet)
{
	//Hmmmm do we implement this?
}

BOOL CSerialEuphoriaHALPDD::SetDCB(LPDCB lpDCB)
{
	if (CSerialPDD::SetDCB(lpDCB)) {
		return SendDCB();
	}
	return FALSE;
}

void CSerialEuphoriaHALPDD::SetDefaultConfiguration()
{
	CSerialPDD::SetDefaultConfiguration();
	SendDCB();
}

BOOL CSerialEuphoriaHALPDD::SendDCB() 		//Sends the relevant parts of the DCB to the real driver
{
	//Send baudrate,bytesize,parity and stopbits using ioctl
	DEBUGMSG(ZONE_INIT,(TEXT("+CSerialEuphoriaHALPDD::SendDCB\r\n") ));
	EUHALSerial::EUHALSerialDCB dcb;
	dcb.DeviceIndex=mDeviceIndex;
	dcb.Baudrate=m_DCB.BaudRate;
	dcb.ByteSize=m_DCB.ByteSize;
	dcb.Parity=m_DCB.Parity;
	dcb.StopBits=m_DCB.StopBits;
	DEBUGMSG(ZONE_INIT,(TEXT("-CSerialEuphoriaHALPDD::SendDCB\r\n") ));
	return DeviceIoControl(g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_SETDCB,&dcb,sizeof(EUHALSerial::EUHALSerialDCB),NULL,0,NULL,NULL);
}
