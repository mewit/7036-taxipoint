#ifndef HALINTERFACE_H
#define HALINTERFACE_H

//Header files containing definitions for communication with the Euphoria HAL to be included in drivers and application to use the HAL

#define mELSEuphoriaHAL_INTERFACE_VERSION		0x100

#include <windef.h>
#include <types.h>
#include <winioctl.h>

//Global
#define MAXDEVICENAME 32
#define MAXPATHROOT 32
#define MAXBOOTAPPPATH 128
#define MAXBOOTAPPPARAMS 32
#define MAXUPDATEFILENAME 128

namespace EUHALDevice
{
	enum Device
	{
		Printer		=0,
		GPS			=1,
		Modem		=2,
		Taximeter	=3,
		RAMModem	=4,
		Beijer		=5,
		Extended	=6,
		BCT			=7,
	};

	enum BoardType
	{
		Karo,
		ArmStoneA8
	};

	enum BootFlags
	{
		Developer = 0x00000001,
		SkipRegistryCheck = 0x00000002,
		InstallationCheck = 0x00000004,

	};

	enum SystemFlags
	{
		UserCanInteract = 0x00000001,				//When enabled user can interact with UI
		TouchScreen	= 0x00000002,					//We have a touch screen
		HWButtons = 0x00000004,						//Has Hardware buttons
		IsEmbedded = 0x00000008,					//Is embedded (CSI/CSDS)
		HasPulseInterface = 0x00000010,				//If defined HAL is capable of retrieving Pulse (ODOMETER values)
	};

	enum ConfigMode
	{
		ConfigNone,
		ConfigCar,		//seats, doors whatnot		was: 500
		ConfigPulse,	//was: 100
		ConfigShutdown,	//forced shutdown, not really a config???		was: 1000
		ConfigOdo,		//was: 500
	};

	enum GPSMode
	{
		NMEA,
		TSIP
	};

	enum DebugConfig
	{
		None = 0x00,
		NoUpdate = 0x01,                    //Apploader will not jump to update application when an USB stick is inserted (Only used for CSI's) 
		//This option is used so we can use the USB stick as a storage device
		ExtendedGPSLog = 0x02,              //This option logs GPS data to find "sticky GPS positions problem)
		LogShutDownState = 0x04,
		LogBiometricDevice = 0x08,
		LogPrinterOutput = 0x10,
		LogGSMOutput = 0x20,
		DebugMode = 0x40,
		DisableDeveloper = 0x20000000,		//Will disable Special debug mode
		IsDeveloper = 0x40000000,			//Special debug mode!!
	};

	//NEVER TOUCH THIS STRUCT, IT MUST REMAIN THE SAME!!!!!!!!!!
	//UPDATER AND APPLOADER DEPEND ON IT!!!

	//Bootflags systemflags and debugconfig flags may be added BUT NEVER CHANGED!!
	struct HALInfo
	{
		DWORD BoardType;					//BoardType
		DWORD BootFlags;
		DWORD SystemFlags;
		DWORD DebugConfig;					//Special bits mostly used for debugging
		WCHAR PStorage[MAXPATHROOT];
		WCHAR SDCard[MAXPATHROOT];
	};

	struct HALRebootInfo
	{
		DWORD PowerOnRebootCount;
		DWORD ExternalRebootCount;
		DWORD BrownOutRebootCount;
		DWORD WatchDogRebootCount;
	};

	//Defines application to start
	enum BootAppFunction
	{
		NoBootApp,
		MainApplication,
		Explorer,
		CalibrateTouchScreen,
		SystemCheck,
		Update,
	};


	struct BootAppInfo
	{
		DWORD BootAppFunction;
		WCHAR Application[MAXBOOTAPPPATH];
		WCHAR Params[MAXBOOTAPPPARAMS];
	};

#define CF_NOPARITY			0
#define CF_ODDPARITY        1
#define CF_EVENPARITY       2
#define CF_MARKPARITY       3
#define CF_SPACEPARITY      4

#define CF_ONESTOPBIT          1			//0
#define CF_ONE5STOPBITS        3			//1
#define CF_TWOSTOPBITS         2			//2

#define CF_HS_NONE				0
#define CF_HS_XOnXOff			1
#define CF_HS_RTS				2
#define CF_HS_RTSXOnXOff		3


	struct DeviceInfo
	{
		TCHAR DeviceName[MAXDEVICENAME];
		int FixedBaudRate;	//
		BYTE FixedBits;			//
		BYTE FixedParity;		//
		BYTE FixedStopBits;		//
		BYTE FixedFlowControl;	//
		int ExtraInfo;				//GPS for example will set NMEA or TSIP mode
	};
}

namespace EUHALSerial
{

#define MAXSERIALMSMQNAME 16

	struct EUHALSerialInit
	{
		TCHAR MSQNameRead[MAXSERIALMSMQNAME];
		TCHAR MSQNameWrite[MAXSERIALMSMQNAME];
	};

	//DCB
	struct EUHALSerialDCB
	{
		int DeviceIndex;
		int Baudrate;
		BYTE ByteSize;
		BYTE Parity;
		BYTE StopBits;
	};

	struct EUHALSerialSET
	{
		DWORD DeviceIndex;
		BOOL value;
	};
}

namespace EUHALSound
{
	//if there is going to be more here,
	//	the ioctl should get its own group,
	//	for now it's in misc:	IOCTL_EUPHORIAHAL_MISC_SETSPEAKERS
	//	direct port from mELSSound for now, i do not know which of the options was relevant for the hal...

	enum SpeakerFlags
	{
		InternalEnabled = 0x1,
		ExternalEnabled = 0x2
	};
}


//Touch driver specific
//Mouse pointer struct for touch driver

namespace EUHALTouch
{
	struct MousePoint
	{
		int x;
		int y;
		BOOL penDown;
	};
}

//Serial port specific

//Display specific

namespace EUHALDisplay
{
	struct Parameters
	{
		int Top;
		int Left;
		int Width;
		int Height;
	};

	enum Output
	{
		WindowsCE,
		External,
		Unknown = -1	//mj: i assume this one is not relevant outside the hal?? (mvz: indeed)
	};

}
//Watchdog specific

namespace EUHALWatchDog
{
	struct Info
	{
		BOOL enabled;
		BOOL inRebootDelay;
		DWORD timeout;
		DWORD rebootDelay;
	};
}

//IO Specific

namespace EUHALIO
{
	enum Function
	{
		Contact,
		RoofLight,
		Aux,
		Alarm,
		Seat,
		Door,
		General
	};

	enum Flags
	{
		Normal		=0x00000000,
		Invert		=0x00000001,
		LatchIOUp	=0x00000002,						//From 0 - 1
		LatchIODown	=0x00000004,						//From 1 - 0
	};

	//A Port is a Function<<16 | index

	struct FunctionInit
	{
		DWORD PortID;
		DWORD Filter;				//n*10ms Max =31		0=No Filter	So a value of 31 means an IO must be in a state for more then 310ms to change
		DWORD Flags;
	};
}

//BCT specific

namespace EUHALBct
{
	struct Parameters
	{
		unsigned int Odo;
		bool IgnitionKey;
	};
}


//Test specific

namespace EUHALTest
{
#define MAXTESTDESCRIPTION 64
	enum TestMode
	{
		None = 0x00,
		Supplier = 0x01,               //So only tests are run needed for autocab
		Internal = 0x02,               //All tests are run (Needed internally for diagnosis)
		Installation = 0x04,           //Tests needed for installation
		Customer = 0x08,               //Tests a customer may run
		HWCheck = 0x10,                //Runs simple Hardware tests (Almost identical to installation)
		All = Supplier | Internal | Installation | Customer | HWCheck,
	};

	// Allowed COM port speeds

	enum SerialTestFlags
	{
		Speed1200 = 0x00000001,
		Speed2400 = 0x00000002,
		Speed4800 = 0x00000004,
		Speed9600 = 0x00000008,
		Speed14400 = 0x00000010,
		Speed19200 = 0x00000020,
		Speed38400 = 0x00000040,
		Speed57600 = 0x00000080,
		Speed115200 = 0x00000100,
		AllowBroken = 0x00000200,      //This is a special Option, this will allow a comport to be tested correctly when ANY data is received
		HasRTSPin = 0x00001000,
		HasCTSPin = 0x00002000,
		HasDTRPin = 0x00004000,
		HasDSRPin = 0x00008000,
		HasDCDPin = 0x00010000,
		HasRINGPin = 0x00020000,
		RTS = 0x00100000,
		DTR = 0x00200000,
	};

	struct SerialTestInfo
	{
		TCHAR DeviceName[MAXDEVICENAME];
		TCHAR Description[MAXTESTDESCRIPTION];
		DWORD SerialTestFlags;		//List of allowable baud rates, available pins and additional flags
	};
}

namespace EUHALButtons
{
	enum HWKeyMasks
	{
		None = 0,
		Power = 0x1,
		F10 = 0x002,
		F11 = 0x004,
		F12 = 0x008,
		F13 = 0x010,
		F14 = 0x020,
		F15 = 0x040,
		F16 = 0x080,
		F17 = 0x100
	};
	enum KeyFunctions
	{
		DayNightKey = 0
	};
}
namespace EUHALHWDevice
{
	enum HWDeviceName
	{
		CEKERNEL =0,							//CE Kernel
		OMEGAATMEL = 1,                         //Omega Atmel chip  (Not updatable)
		MAGPS3COM = 2,                          //Zwarte kastje (Updatable)
		MA7004 = 3,                             //Atmel for CSI (Updatable)
		FPGACSI = 4,                            //FPGA Used in CSI for video display (Updatable)
		FPGA7025600 = 5,                        //Lattice MachX02 on ArmstoneA8 Board (Updatable we hope)
	};

	enum HWFlags
	{
		NotAvailable = 0x00,            //First 3 bits contains the state the device is in (When value = 0 means device is not available)
		State_Detecting = 0x01,			//Busy detecting device (Version not yet available)
		State_Initializing = 0x02,      //Device is initializing
		State_Programming = 0x03,       //Device is programming
		State_Ready = 0x04,             //Device is ready for action
		State_Mask = 0x07,
		Updatable = 0x08,               //When set device is updatable
		ConcurrentUpdateEnabled = 0x10, //When set device is updatable
	};

	struct HWDeviceInfo
	{
		DWORD HWDevice;
		DWORD Version;
		DWORD Flags;
		DWORD Progress;
		HWDeviceInfo( HWDeviceName deviceName = (HWDeviceName)-1, HWFlags flags = NotAvailable ) :HWDevice(deviceName),Version(0),Flags(flags),Progress(0) {;}
		inline HWFlags State() const
		{
			return static_cast<HWFlags>(Flags & State_Mask);
		}
		inline void State( HWFlags flags )
		{
			Flags = ((Flags & ~State_Mask) | flags);
		}
	};

	struct HWDeviceUpdate
	{
		DWORD HWDevice;
		DWORD Version;
		TCHAR UpdateFileName[MAXUPDATEFILENAME];
	};
}

//Global events
#define GLOBAL_WATCHDOG_EVENTNAME TEXT("EUPHORIAHAL_WATCHDOG")
#define GLOBAL_IO_EVENTNAME TEXT("EUPHORIAHAL_IO")
#define GLOBAL_TOUCH_EVENTNAME TEXT("EUPHORIAHAL_TOUCH")
#define GLOBAL_PHONE_EVENTNAME TEXT("EUPHORIAHAL_PHONE")
#define GLOBAL_POWER_EVENTNAME TEXT("EUPHORIAHAL_POWER")

#define PORTNAME_DEVICE_EUPHORIA_HAL	TEXT("EUP1:")
#define FILE_DEVICE_EUPHORIA_HAL 43767

//Global codes
//Returns HALDeviceInfo 
#define IOCTL_EUPHORIAHAL_INFO						CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0800, METHOD_BUFFERED, FILE_ANY_ACCESS)
//Returns a DeviceName for a given Device
#define IOCTL_EUPHORIAHAL_GETDEVICEINFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0801, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Returns an BootAppFunction, using ButtonStates
#define IOCTL_EUPHORIAHAL_GETBOOTAPPFUNCTIONBYKEY	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0810, METHOD_BUFFERED, FILE_ANY_ACCESS)
//Returns an application to start using a BootAppFunction
#define IOCTL_EUPHORIAHAL_GETBOOTAPPBYFUNCTION		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0811, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Sends debug data (Send 1 int)
#define IOCTL_EUPHORIAHAL_SETDEBUGDATA				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x08ff, METHOD_BUFFERED, FILE_ANY_ACCESS)


//Misc control codes
//Will shutdown the Terminal
#define IOCTL_EUPHORIAHAL_MISC_SHUTDOWN				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0900, METHOD_BUFFERED, FILE_ANY_ACCESS)
//Will reboot the Terminal
#define IOCTL_EUPHORIAHAL_MISC_REBOOT				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0901, METHOD_BUFFERED, FILE_ANY_ACCESS)
//Will sound a beep (used for debug only)
#define IOCTL_EUPHORIAHAL_MISC_BEEP					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0902, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_USEPULSE				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0903, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_GETODO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0904, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_GETPULSE				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0905, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_GETKFACTOR			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0906, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_SETKFACTOR			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0907, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_SETCONFIGMODE		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0908, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_MISC_SETTIME				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0909, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Watchdog control codes
#define IOCTL_EUPHORIAHAL_WATCHDOG_INFO						CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0940, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_ENABLE					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0941, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_DISABLE					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0942, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_KICK						CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0943, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_SETTIMEOUT				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0944, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_SETREBOOTDELAY			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0945, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_GETINFO					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0946, METHOD_BUFFERED, FILE_ANY_ACCESS)
//When calling below IOCT application will forcibly reboot or shutdown the device in x amount of seconds
#define IOCTL_EUPHORIAHAL_WATCHDOG_FORCEREBOOTINSEC			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0947, METHOD_BUFFERED, FILE_ANY_ACCESS)


#define IOCTL_EUPHORIAHAL_WATCHDOG_GETFORCEDSHUTDOWNINSEC	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0950, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Remember to add the current delay to this delay (Add GETFORCEDSHUTDOWNINSEC internally)
#define IOCTL_EUPHORIAHAL_WATCHDOG_POSTPONEFORCEDSHUTDOWN	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0951, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Delay is in minutes
#define IOCTL_EUPHORIAHAL_WATCHDOG_GETSHUTDOWNDELAY			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0952, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_WATCHDOG_SETSHUTDOWNDELAY			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0953, METHOD_BUFFERED, FILE_ANY_ACCESS)


//Button interface control code
#define IOCTL_EUPHORIAHAL_BUTTON_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0980, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_BUTTON_GETSTATE			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0981, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_BUTTON_GETKEYFORFUNCTION	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0982, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Interface for HW Devices (Updating internal hardware and get version numbers)
#define IOCTL_EUPHORIAHAL_HW_INFO					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0990, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_HW_DEVICEINFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0991, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_HW_DEVICEUPDATE			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0992, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Interface for BCT

#define IOCTL_EUPHORIAHAL_BCT_SETHASBCT				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x09a0, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_BCT_SETDATA				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x09a1, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_BCT_GETHASBCT				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x09a2, METHOD_BUFFERED, FILE_ANY_ACCESS)

//video control codes
#define IOCTL_EUPHORIAHAL_VIDEO_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a00, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_GETBRIGHTNESS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a01, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_SETBRIGHTNESS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a02, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_GETBORDERCOLOR		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a03, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_SETBORDERCOLOR		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a04, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_GETDISPLAY			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a05, METHOD_BUFFERED, FILE_ANY_ACCESS)		//Set display to CE or ext
#define IOCTL_EUPHORIAHAL_VIDEO_SETDISPLAY			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a06, METHOD_BUFFERED, FILE_ANY_ACCESS)		//Set display to CE or ext

#define IOCTL_EUPHORIAHAL_VIDEO_GETPARAMETERS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a20, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_SETPARAMETERS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a21, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_STOREPARAMETERS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a22, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_GETENABLED			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a23, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_SETENABLED			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a24, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_EUPHORIAHAL_VIDEO_HASSECONDSCREEN		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a40, METHOD_BUFFERED, FILE_ANY_ACCESS)		//does the platform have a second screen
#define IOCTL_EUPHORIAHAL_VIDEO_SECONDSCREENAVAIL	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a41, METHOD_BUFFERED, FILE_ANY_ACCESS)		//is the second screen available this session
#define IOCTL_EUPHORIAHAL_VIDEO_ISSECONDSCREENENABLED	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a42, METHOD_BUFFERED, FILE_ANY_ACCESS)	//is the screen powered on
#define IOCTL_EUPHORIAHAL_VIDEO_SECONDSCREENENABLE	CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a43, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_VIDEO_SECONDSCREENDIM		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0a44, METHOD_BUFFERED, FILE_ANY_ACCESS)


//Touch control codes
#define IOCTL_EUPHORIAHAL_TOUCH_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0b00, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_TOUCH_GETPOINT			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0b01, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Serial interface control code
#define IOCTL_EUPHORIAHAL_SERIAL_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c00, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Returns false when no HW is connected
#define IOCTL_EUPHORIAHAL_SERIAL_DEVICEEXIST		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c01, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_EUPHORIAHAL_SERIAL_SETDCB				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c10, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_INIT				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c11, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_FREE				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c12, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_CANCELXMIT			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c13, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_CANCELRECEIVE		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c14, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_SETDTR				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c20, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_SETRTS				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c21, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SERIAL_GETMODEMSTATUS		CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0c22, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Phone interface control code
#define IOCTL_EUPHORIAHAL_PHONE_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d00, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_PHONE_RESET				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d10, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Sound interface
#define IOCTL_EUPHORIAHAL_SOUND_INFO				CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d80, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SOUND_SETSPEAKERS			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d81, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SOUND_SETVOLUME			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d82, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_SOUND_GETVOLUME			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0d83, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Test interface control code
#define IOCTL_EUPHORIAHAL_TEST_INFO					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0f00, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_TEST_SETTESTMODE			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0f01, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Executes a internal HAL IO Test returns TRUE when successful
#define IOCTL_EUPHORIAHAL_TEST_HALIOTEST			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0f10, METHOD_BUFFERED, FILE_ANY_ACCESS)

//Returns amount of serial devices to test
#define IOCTL_EUPHORIAHAL_TEST_SERIALCOUNT			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0f20, METHOD_BUFFERED, FILE_ANY_ACCESS)
//Returns info for serial port to test
#define IOCTL_EUPHORIAHAL_TEST_SERIALINFO			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0f21, METHOD_BUFFERED, FILE_ANY_ACCESS)

//mjtodo: not sure if we need this, possibly hide it from the CS?
//read / write 16 bytes storage (input = bank nr.)
// 0..5 = read/write (free storage), 6 = readonly, 7 = internal data
#define IOCTL_EUPHORIAHAL_TEST_READSTORAGE			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0ffe, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_TEST_WRITESTORAGE			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x0fff, METHOD_BUFFERED, FILE_ANY_ACCESS)


//fail....
//max ctrl code is 0xfff

//IO interface control code
#define IOCTL_EUPHORIAHAL_IO_INFO					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1000, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_IO_FUNCTIONCOUNT			CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1001, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_IO_INIT					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1010, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_IO_GET					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1011, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_IO_SET					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1012, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_EUPHORIAHAL_IO_CLR					CTL_CODE(FILE_DEVICE_EUPHORIA_HAL,  0x1013, METHOD_BUFFERED, FILE_ANY_ACCESS)


#endif //#ifndef HALINTERFACE_H
