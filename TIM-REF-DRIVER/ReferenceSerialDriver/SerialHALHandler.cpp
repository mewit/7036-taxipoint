#include "SerialHALHandler.h"
#include "SerialHALBase.h"

#define MAXREADSIZE 1024

CSerialDeviceHandler* g_DeviceHandler = CSerialDeviceHandler::CreateDeviceHandler();

CSerialDeviceHandler::CSerialDeviceHandler()
 : mGlobalDeviceThread(INVALID_HANDLE_VALUE)
 , mGlobalDeviceThreadID(0)
 , mDeviceEvent(NULL)
 , mHAL(NULL)
 , mGlobalDeviceThreadRunning(false)
 , mDevicesChanged(false)
{
	mDeviceHandlerInitialized = initDeviceHandler();
}

CSerialDeviceHandler::~CSerialDeviceHandler()
{
	if (mHAL) {
		CloseHandle(mHAL);
	}
	if( mGlobalDeviceThreadRunning ) {
		mGlobalDeviceThreadRunning = false;
		SetEvent( mDeviceEvent );
		WaitForSingleObject( mGlobalDeviceThread, 1000 );
	}
}

void CSerialDeviceHandler::addDevice(CSerialEuphoriaHALPDD* device)
{
	{
		CSHelper cs(mDeviceMutex);
		mDevices.push_back(device);
		mDevicesChanged=true;
	}
	SetEvent(mDeviceEvent);
}

void CSerialDeviceHandler::removeDevice(CSerialEuphoriaHALPDD* device)
{
	{
		CSHelper cs(mDeviceMutex);
		for (unsigned int i=0;i<mDevices.size();i++) {
			if (mDevices[i] == device) {
				mDevices.erase(mDevices.begin()+i);
				mDevicesChanged=true;
				break;
			}
		}
	}
	SetEvent(mDeviceEvent);
}

bool CSerialDeviceHandler::initDeviceHandler()
{
#if DEBUG
//	dpCurSettings.ulZoneMask|= 0xffff;//ZONE_INIT;
#endif
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::initDeviceHandler\r\n") ));

	InitializeCriticalSection(&mDeviceMutex);
	CSHelper cs(mDeviceMutex);
	mHAL = CreateFile( PORTNAME_DEVICE_EUPHORIA_HAL, GENERIC_WRITE|GENERIC_READ, 0, NULL, OPEN_EXISTING , FILE_ATTRIBUTE_NORMAL, NULL );
	mDeviceEvent=CreateEvent(NULL,false,false,NULL);
	if (mDeviceEvent) {
		mGlobalDeviceThreadRunning=true;
		mWaitEvents[0]=mDeviceEvent;
		mGlobalDeviceThread=CreateThread(NULL, 0, &CSerialDeviceHandler::deviceThreadStart, this, 0, &mGlobalDeviceThreadID);
		if (mGlobalDeviceThread != INVALID_HANDLE_VALUE) {
			return true;
		}
	}
	return false;
}

DWORD CSerialDeviceHandler::deviceThreadStart(LPVOID lpParameter)
{
	return ((CSerialDeviceHandler*)lpParameter)->deviceThread();
}

DWORD CSerialDeviceHandler::deviceThread()
{
	MSGQUEUEINFO qi;
	qi.dwSize=sizeof(MSGQUEUEINFO);
	DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::deviceThread starting..\r\n") ));
	while( mGlobalDeviceThreadRunning ) {
		unsigned int deviceCount;
		{
			CSHelper cs(mDeviceMutex);
			deviceCount = mDevices.size();
			if( mDevicesChanged ) {
				for( unsigned int i = 0; i < deviceCount; i++ ) {
					mWaitEvents[i+1] = mDevices[i]->mMSQReadHandle;
				}
				mDevicesChanged = false;
			}
			INTERRUPT_TYPE intr = INTR_NONE;
			for( unsigned int i = 0; i < deviceCount; i++ ) {
				intr = INTR_NONE;
				//Check all read queues if we have data...
				if( mDevices[i]->mWriteEnabled ) {
					intr = (INTERRUPT_TYPE)(intr | INTR_TX);
					//mDevices[i]->NotifyPDDInterrupt(INTR_TX);
					//DEBUGMSG(ZONE_INIT,(TEXT("CSerialEuphoriaHALPDD::deviceThread INTR_TX..\r\n") ));
				}
				if( GetMsgQueueInfo(mDevices[i]->mMSQReadHandle,&qi) ) {
					if( qi.dwCurrentMessages > 0 ) {
						intr = (INTERRUPT_TYPE)(intr | INTR_RX);
						//mDevices[i]->NotifyPDDInterrupt(INTR_RX);
						//DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD::deviceThread INTR_RX: %d..\r\n"), qi.dwCurrentMessages ) );
					}
				}
				if( mDevices[i]->mBackupBuf ) {
					intr = (INTERRUPT_TYPE)(intr | INTR_RX);
					//DEBUGMSG( ZONE_INIT, (TEXT("CSerialEuphoriaHALPDD::deviceThread INTR_RX: (backup)..\r\n") ) );
				}
				if( intr != INTR_NONE ) {
					mDevices[i]->NotifyPDDInterrupt( intr );
				}
			}
		}
		WaitForMultipleObjects(deviceCount+1,mWaitEvents,FALSE,20);
	}
	return 0;
}


CSerialDeviceHandler* CSerialDeviceHandler::CreateDeviceHandler()
{
	return new CSerialDeviceHandler();
}


CSerialPDD * CreateSerialObject(LPTSTR lpActivePath, PVOID pMdd,PHWOBJ pHwObj, DWORD DeviceArrayIndex)
{
	//Check if global handler is initialized
	if (DeviceIoControl( g_DeviceHandler->mHAL,IOCTL_EUPHORIAHAL_SERIAL_DEVICEEXIST,&DeviceArrayIndex,sizeof(DWORD),NULL,0,NULL,NULL)) {
		CSerialPDD* pSerialPDD = new CSerialEuphoriaHALPDD(lpActivePath,pMdd, pHwObj,DeviceArrayIndex);
		if (pSerialPDD && (pSerialPDD->Init() != TRUE)) {
			delete pSerialPDD;
			pSerialPDD = NULL;
		}
		return pSerialPDD;
	}
	return NULL;
}

void DeleteSerialObject(CSerialPDD * pSerialPDD)
{
	if (pSerialPDD) {
		delete pSerialPDD;
	}
}
