#ifndef SERIALHALBASE_H
#define SERIALHALBASE_H

#define USE_NEW_SERIAL_MODEL

#include <HALInterface.h>
#include "SerialHALHandler.h"

#if (_WIN32_WCE == 0x500)
#ifdef _DEBUG
#include <OmegaDebug.h>
#endif
#endif

//#include <pnotify.h>
//#include <notifdev.h>
//#include <ddkreg.h>
#include <vector>

#define MAXSERIALDEVICES 64			//Maximum of virtual serial ports we accept

extern CSerialDeviceHandler* g_DeviceHandler;

class CSerialEuphoriaHALPDD : public CSerialPDD
{
protected:
	HANDLE mMSQReadHandle;
	HANDLE mMSQWriteHandle;
	EUHALSerial::EUHALSerialInit mHALInitializationData;
	int mDeviceIndex;


	char* mBackupBuf;
	ULONG mBackupRead;

public:
	CSerialEuphoriaHALPDD(LPTSTR lpActivePath, PVOID pMdd, PHWOBJ pHwObj , int deviceArrayIndex);
	virtual ~CSerialEuphoriaHALPDD();
	bool mWriteEnabled;

//	virtual BOOL Init();
//	virtual void PostInit() ;
public:
//	virtual BOOL    Open();
//	virtual BOOL    Close();
//	virtual BOOL    Ioctl(DWORD dwCode,PBYTE pBufIn,DWORD dwLenIn,PBYTE pBufOut,DWORD dwLenOut,PDWORD pdwActualOut);
public:
	virtual void    SerialRegisterBackup();//=0;
	virtual void    SerialRegisterRestore();//=0;
	// 
	//  Tx Function.
	virtual BOOL    InitXmit(BOOL bInit);//=0;
	virtual void    XmitInterruptHandler(PUCHAR pTxBuffer, ULONG *pBuffLen);//= 0;
	virtual void    XmitComChar(UCHAR ComChar);// = 0;
	virtual BOOL    EnableXmitInterrupt(BOOL bEnable);//= 0;
	virtual BOOL    CancelXmit();// = 0 ;
	//
	//  Rx Function.
	virtual BOOL    InitReceive(BOOL bInit);// = 0;
	virtual ULONG   ReceiveInterruptHandler(PUCHAR pRxBuffer,ULONG *pBufflen);// = 0;
	virtual ULONG   CancelReceive();// = 0;
	//
	//  Modem
	virtual BOOL    InitModem(BOOL bInit);// = 0;
	virtual void    ModemInterruptHandler();//= 0; // This is Used to Indicate Modem Signal Changes.
	virtual ULONG   GetModemStatus();// = 0;
	virtual void    SetDTR(BOOL bSet);//= 0;
	virtual void    SetRTS(BOOL bSet);//= 0;
//	virtual BOOL    IsCTSOff() {  return ((GetModemStatus() & MS_CTS_ON)==0) ; };
//	virtual BOOL    IsDSROff() {  return ((GetModemStatus() & MS_DSR_ON)==0) ; };
	//  Line Function
	virtual BOOL    InitLine(BOOL bInit);// = 0;
	virtual void    LineInterruptHandler();// = 0;
	virtual void    SetBreak(BOOL bSet);// = 0 ;   

	//Following 4 commands are not used, SendDCB is used instead to send all at once
	virtual BOOL    SetBaudRate(ULONG BaudRate,BOOL bIrModule) { return TRUE;}
	virtual BOOL    SetByteSize(ULONG ByteSize) { return TRUE;}
	virtual BOOL    SetParity(ULONG Parity) { return TRUE;}
	virtual BOOL    SetStopBits(ULONG StopBits) { return TRUE;}

	virtual BOOL    SetDCB(LPDCB lpDCB);
	virtual void    SetDefaultConfiguration();

public:
//	virtual BOOL    GetDivisorOfRate(ULONG BaudRate,PULONG pulDivisor);
public:
//	virtual void    SetReceiveError(ULONG);
//	virtual ULONG   GetReceivedError() ;

private:
	BOOL SendDCB();		//Sends the relevant parts of the DCB to the real driver

	friend class CSerialDeviceHandler;
};

#endif //SERIALHALBASE_H
