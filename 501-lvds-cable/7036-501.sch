<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N" xrefpart="/%S.%C%R">
<libraries>
<library name="frames_Custom">
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="224.155" y2="24.13" width="0.1016" layer="94"/>
<wire x1="224.155" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="224.155" y2="8.89" width="0.1016" layer="94"/>
<wire x1="224.155" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="224.155" y2="13.97" width="0.1016" layer="94"/>
<wire x1="224.155" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="224.155" y2="19.05" width="0.1016" layer="94"/>
<wire x1="224.155" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="224.155" y1="19.05" x2="224.155" y2="24.13" width="0.1016" layer="94"/>
<wire x1="224.155" y1="13.97" x2="224.155" y2="19.05" width="0.1016" layer="94"/>
<wire x1="224.155" y1="8.89" x2="224.155" y2="13.97" width="0.1016" layer="94"/>
<wire x1="238.76" y1="3.81" x2="238.76" y2="8.89" width="0.1016" layer="94"/>
<text x="225.425" y="15.24" size="2.1844" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="225.425" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="239.776" y="4.953" size="1.27" layer="94" font="vector">Sheet:</text>
<text x="225.425" y="19.685" size="2.54" layer="94">&gt;VALUE</text>
<text x="215.9" y="22.225" size="1.27" layer="94">Drawing</text>
<text x="215.9" y="20.32" size="1.27" layer="94">number</text>
<text x="246.38" y="19.685" size="2.54" layer="94">-</text>
<text x="215.9" y="15.24" size="1.27" layer="94">File</text>
<text x="215.9" y="10.16" size="1.27" layer="94">Date</text>
<text x="247.015" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="248.92" y="19.685" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-jae">
<packages>
<package name="FI-XB30SSL-15">
<description>&lt;b&gt;Connector FI-XB30SSL-HF15&lt;/b&gt;&lt;p&gt;
Source: http://jae-connector.com/en/pdf/SJ038317.pdf</description>
<wire x1="-21" y1="-3.5" x2="-18.825" y2="-3.5" width="0" layer="20"/>
<wire x1="-18.825" y1="-3.5" x2="-18.825" y2="-1.2" width="0" layer="20"/>
<wire x1="-18.825" y1="-1.2" x2="-18.825" y2="0" width="0" layer="20" curve="-180"/>
<wire x1="-18.825" y1="0" x2="18.825" y2="0" width="0" layer="20"/>
<wire x1="18.825" y1="-1.2" x2="18.825" y2="0" width="0" layer="20" curve="180"/>
<wire x1="18.825" y1="-1.2" x2="18.825" y2="-3.5" width="0" layer="20"/>
<wire x1="18.825" y1="-3.5" x2="21" y2="-3.5" width="0" layer="20"/>
<wire x1="-19.9367" y1="-2.7875" x2="-19.9367" y2="-1.6936" width="0.1016" layer="21"/>
<wire x1="-19.9367" y1="-1.6936" x2="-18.5958" y2="-1.6936" width="0.1016" layer="21"/>
<wire x1="-19.9367" y1="-2.7875" x2="-18.5958" y2="-2.7875" width="0.1016" layer="21"/>
<wire x1="-18.5958" y1="-2.7875" x2="-18.5958" y2="-2.9992" width="0.1016" layer="21"/>
<wire x1="-18.5958" y1="-2.9992" x2="-18.243" y2="-2.9992" width="0.1016" layer="21"/>
<wire x1="-18.243" y1="-2.9992" x2="-18.243" y2="-3.7049" width="0.1016" layer="21"/>
<wire x1="-18.243" y1="-3.7049" x2="18.278" y2="-3.7049" width="0.1016" layer="21"/>
<wire x1="18.278" y1="-3.7049" x2="18.278" y2="-3.0698" width="0.1016" layer="21"/>
<wire x1="18.278" y1="-3.0698" x2="18.6309" y2="-3.0698" width="0.1016" layer="21"/>
<wire x1="18.6309" y1="-3.0698" x2="18.6309" y2="-0.3175" width="0.1016" layer="21"/>
<wire x1="18.6309" y1="-0.3175" x2="-18.5958" y2="-0.3175" width="0.1016" layer="21"/>
<wire x1="-18.5958" y1="-0.3175" x2="-18.5958" y2="-1.6936" width="0.1016" layer="21"/>
<wire x1="-18.5958" y1="-1.6936" x2="-18.5958" y2="-2.7875" width="0.1016" layer="21"/>
<wire x1="-15.2084" y1="-0.3528" x2="-15.2084" y2="-1.0938" width="0.1016" layer="21"/>
<wire x1="-15.2084" y1="-1.0938" x2="-15.032" y2="-1.2702" width="0.1016" layer="21" curve="90"/>
<wire x1="-15.032" y1="-1.2702" x2="15.1376" y2="-1.2702" width="0.1016" layer="21"/>
<wire x1="15.1376" y1="-1.2702" x2="15.2787" y2="-1.1291" width="0.1016" layer="21" curve="90"/>
<wire x1="15.2787" y1="-1.1291" x2="15.2787" y2="-0.3528" width="0.1016" layer="21"/>
<wire x1="18.6662" y1="-1.6936" x2="19.9718" y2="-1.6936" width="0.1016" layer="21"/>
<wire x1="19.9718" y1="-1.6936" x2="19.9718" y2="-2.7875" width="0.1016" layer="21"/>
<wire x1="19.9718" y1="-2.7875" x2="18.6662" y2="-2.7875" width="0.1016" layer="21"/>
<wire x1="-15.7023" y1="-0.2822" x2="-15.7023" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-15.7023" y1="1.6585" x2="-15.3142" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-15.3142" y1="1.6585" x2="-15.3142" y2="-0.2469" width="0.1016" layer="51"/>
<wire x1="-14.7496" y1="-0.2822" x2="-14.7496" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-14.7496" y1="0.2471" x2="-14.8202" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-14.8202" y1="0.3177" x2="-14.8202" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-14.8202" y1="0.5294" x2="-14.5732" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-14.5732" y1="0.5294" x2="-14.5732" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-14.5732" y1="1.6585" x2="-14.432" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-14.432" y1="1.6585" x2="-14.432" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-14.432" y1="0.5294" x2="-14.2203" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-14.2203" y1="0.5294" x2="-14.2203" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-14.2203" y1="0.3177" x2="-14.2909" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-14.2909" y1="0.2471" x2="-14.2909" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-13.7616" y1="-0.2822" x2="-13.7616" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-13.7616" y1="0.2471" x2="-13.8322" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-13.8322" y1="0.3177" x2="-13.8322" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-13.8322" y1="0.5294" x2="-13.5852" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-13.5852" y1="0.5294" x2="-13.5852" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-13.5852" y1="1.6585" x2="-13.444" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-13.444" y1="1.6585" x2="-13.444" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-13.444" y1="0.5294" x2="-13.2323" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-13.2323" y1="0.5294" x2="-13.2323" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-13.2323" y1="0.3177" x2="-13.3029" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-13.3029" y1="0.2471" x2="-13.3029" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-12.7383" y1="-0.2822" x2="-12.7383" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-12.7383" y1="0.2471" x2="-12.8089" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-12.8089" y1="0.3177" x2="-12.8089" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-12.8089" y1="0.5294" x2="-12.5619" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-12.5619" y1="0.5294" x2="-12.5619" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-12.5619" y1="1.6585" x2="-12.4207" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-12.4207" y1="1.6585" x2="-12.4207" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-12.4207" y1="0.5294" x2="-12.209" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-12.209" y1="0.5294" x2="-12.209" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-12.209" y1="0.3177" x2="-12.2796" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-12.2796" y1="0.2471" x2="-12.2796" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-14.0792" y1="-1.8348" x2="-12.7384" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-12.7384" y1="-1.8348" x2="-12.7384" y2="-3.4226" width="0.1016" layer="21"/>
<wire x1="-12.7384" y1="-3.4226" x2="-14.0792" y2="-3.4226" width="0.1016" layer="21"/>
<wire x1="-14.0792" y1="-3.4226" x2="-14.0792" y2="-3.2109" width="0.1016" layer="21"/>
<wire x1="-14.0792" y1="-3.2109" x2="-12.9501" y2="-3.2109" width="0.1016" layer="21"/>
<wire x1="-12.9501" y1="-3.2108" x2="-12.9501" y2="-2.7522" width="0.1016" layer="21"/>
<wire x1="-12.9501" y1="-2.7522" x2="-13.7617" y2="-2.7522" width="0.1016" layer="21"/>
<wire x1="-13.7617" y1="-2.7522" x2="-13.7617" y2="-2.5052" width="0.1016" layer="21"/>
<wire x1="-13.7617" y1="-2.5052" x2="-12.9148" y2="-2.5052" width="0.1016" layer="21"/>
<wire x1="-12.9148" y1="-2.5052" x2="-12.9148" y2="-2.0112" width="0.1016" layer="21"/>
<wire x1="-12.9148" y1="-2.0112" x2="-14.0792" y2="-2.0112" width="0.1016" layer="21"/>
<wire x1="-14.0792" y1="-2.0112" x2="-14.0792" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-12.0679" y1="-1.8348" x2="-11.5034" y2="-3.4226" width="0.1016" layer="21"/>
<wire x1="-11.5034" y1="-3.4226" x2="-11.2916" y2="-3.4226" width="0.1016" layer="21"/>
<wire x1="-11.2916" y1="-3.4226" x2="-10.7271" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-10.7271" y1="-1.8348" x2="-10.9388" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-10.9388" y1="-1.8348" x2="-11.0799" y2="-2.2229" width="0.1016" layer="21"/>
<wire x1="-11.0799" y1="-2.2229" x2="-11.7151" y2="-2.2229" width="0.1016" layer="21"/>
<wire x1="-11.7151" y1="-2.2229" x2="-11.8562" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-11.8562" y1="-1.8348" x2="-12.0679" y2="-1.8348" width="0.1016" layer="21"/>
<wire x1="-11.3975" y1="-3.1051" x2="-11.1858" y2="-2.4699" width="0.1016" layer="21"/>
<wire x1="-11.1858" y1="-2.4699" x2="-11.6092" y2="-2.4699" width="0.1016" layer="21"/>
<wire x1="-11.6092" y1="-2.4699" x2="-11.3975" y2="-3.1051" width="0.1016" layer="21"/>
<wire x1="-10.0566" y1="-3.4226" x2="-9.8449" y2="-3.4226" width="0.1016" layer="21"/>
<wire x1="-9.8449" y1="-3.4226" x2="-9.8449" y2="-2.5052" width="0.1016" layer="21"/>
<wire x1="-9.8449" y1="-2.5052" x2="-8.9275" y2="-2.4699" width="0.1016" layer="21" curve="-175.493109"/>
<wire x1="-8.9275" y1="-2.4699" x2="-8.7511" y2="-2.4699" width="0.1016" layer="21"/>
<wire x1="-10.0566" y1="-3.4226" x2="-10.0566" y2="-2.4699" width="0.1016" layer="21"/>
<wire x1="-10.0566" y1="-2.4699" x2="-8.7158" y2="-2.4699" width="0.1016" layer="21" curve="-180"/>
<wire x1="14.2554" y1="-0.2822" x2="14.2554" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="14.2554" y1="0.2471" x2="14.1848" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="14.1848" y1="0.3177" x2="14.1848" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="14.1848" y1="0.5294" x2="14.4318" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="14.4318" y1="0.5294" x2="14.4318" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="14.4318" y1="1.6585" x2="14.573" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="14.573" y1="1.6585" x2="14.573" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="14.573" y1="0.5294" x2="14.7847" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="14.7847" y1="0.5294" x2="14.7847" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="14.7847" y1="0.3177" x2="14.7141" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="14.7141" y1="0.2471" x2="14.7141" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="13.2674" y1="-0.2822" x2="13.2674" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="13.2674" y1="0.2471" x2="13.1968" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="13.1968" y1="0.3177" x2="13.1968" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="13.1968" y1="0.5294" x2="13.4438" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="13.4438" y1="0.5294" x2="13.4438" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="13.4438" y1="1.6585" x2="13.585" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="13.585" y1="1.6585" x2="13.585" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="13.585" y1="0.5294" x2="13.7967" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="13.7967" y1="0.5294" x2="13.7967" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="13.7967" y1="0.3177" x2="13.7261" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="13.7261" y1="0.2471" x2="13.7261" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="15.2787" y1="-0.2469" x2="15.2787" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="15.2787" y1="1.6585" x2="15.6669" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="15.6669" y1="1.6585" x2="15.6669" y2="-0.2469" width="0.1016" layer="51"/>
<wire x1="-11.7496" y1="-0.2822" x2="-11.7496" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-11.7496" y1="0.2471" x2="-11.8202" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-11.8202" y1="0.3177" x2="-11.8202" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-11.8202" y1="0.5294" x2="-11.5732" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-11.5732" y1="0.5294" x2="-11.5732" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-11.5732" y1="1.6585" x2="-11.432" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-11.432" y1="1.6585" x2="-11.432" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-11.432" y1="0.5294" x2="-11.2203" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-11.2203" y1="0.5294" x2="-11.2203" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-11.2203" y1="0.3177" x2="-11.2909" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-11.2909" y1="0.2471" x2="-11.2909" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-10.7616" y1="-0.2822" x2="-10.7616" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-10.7616" y1="0.2471" x2="-10.8322" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-10.8322" y1="0.3177" x2="-10.8322" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-10.8322" y1="0.5294" x2="-10.5852" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-10.5852" y1="0.5294" x2="-10.5852" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-10.5852" y1="1.6585" x2="-10.444" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-10.444" y1="1.6585" x2="-10.444" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-10.444" y1="0.5294" x2="-10.2323" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-10.2323" y1="0.5294" x2="-10.2323" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-10.2323" y1="0.3177" x2="-10.3029" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-10.3029" y1="0.2471" x2="-10.3029" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-9.7383" y1="-0.2822" x2="-9.7383" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-9.7383" y1="0.2471" x2="-9.8089" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-9.8089" y1="0.3177" x2="-9.8089" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-9.8089" y1="0.5294" x2="-9.5619" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-9.5619" y1="0.5294" x2="-9.5619" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-9.5619" y1="1.6585" x2="-9.4207" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-9.4207" y1="1.6585" x2="-9.4207" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-9.4207" y1="0.5294" x2="-9.209" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-9.209" y1="0.5294" x2="-9.209" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-9.209" y1="0.3177" x2="-9.2796" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-9.2796" y1="0.2471" x2="-9.2796" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-8.7496" y1="-0.2822" x2="-8.7496" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-8.7496" y1="0.2471" x2="-8.8202" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-8.8202" y1="0.3177" x2="-8.8202" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-8.8202" y1="0.5294" x2="-8.5732" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-8.5732" y1="0.5294" x2="-8.5732" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-8.5732" y1="1.6585" x2="-8.432" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-8.432" y1="1.6585" x2="-8.432" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-8.432" y1="0.5294" x2="-8.2203" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-8.2203" y1="0.5294" x2="-8.2203" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-8.2203" y1="0.3177" x2="-8.2909" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-8.2909" y1="0.2471" x2="-8.2909" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-7.7616" y1="-0.2822" x2="-7.7616" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-7.7616" y1="0.2471" x2="-7.8322" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-7.8322" y1="0.3177" x2="-7.8322" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-7.8322" y1="0.5294" x2="-7.5852" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-7.5852" y1="0.5294" x2="-7.5852" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-7.5852" y1="1.6585" x2="-7.444" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-7.444" y1="1.6585" x2="-7.444" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-7.444" y1="0.5294" x2="-7.2323" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-7.2323" y1="0.5294" x2="-7.2323" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-7.2323" y1="0.3177" x2="-7.3029" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-7.3029" y1="0.2471" x2="-7.3029" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-6.7383" y1="-0.2822" x2="-6.7383" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-6.7383" y1="0.2471" x2="-6.8089" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-6.8089" y1="0.3177" x2="-6.8089" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-6.8089" y1="0.5294" x2="-6.5619" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-6.5619" y1="0.5294" x2="-6.5619" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-6.5619" y1="1.6585" x2="-6.4207" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-6.4207" y1="1.6585" x2="-6.4207" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-6.4207" y1="0.5294" x2="-6.209" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-6.209" y1="0.5294" x2="-6.209" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-6.209" y1="0.3177" x2="-6.2796" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-6.2796" y1="0.2471" x2="-6.2796" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-5.7496" y1="-0.2822" x2="-5.7496" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-5.7496" y1="0.2471" x2="-5.8202" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-5.8202" y1="0.3177" x2="-5.8202" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-5.8202" y1="0.5294" x2="-5.5732" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-5.5732" y1="0.5294" x2="-5.5732" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-5.5732" y1="1.6585" x2="-5.432" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-5.432" y1="1.6585" x2="-5.432" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-5.432" y1="0.5294" x2="-5.2203" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-5.2203" y1="0.5294" x2="-5.2203" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-5.2203" y1="0.3177" x2="-5.2909" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-5.2909" y1="0.2471" x2="-5.2909" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-4.7616" y1="-0.2822" x2="-4.7616" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-4.7616" y1="0.2471" x2="-4.8322" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-4.8322" y1="0.3177" x2="-4.8322" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-4.8322" y1="0.5294" x2="-4.5852" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-4.5852" y1="0.5294" x2="-4.5852" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-4.5852" y1="1.6585" x2="-4.444" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-4.444" y1="1.6585" x2="-4.444" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-4.444" y1="0.5294" x2="-4.2323" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-4.2323" y1="0.5294" x2="-4.2323" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-4.2323" y1="0.3177" x2="-4.3029" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-4.3029" y1="0.2471" x2="-4.3029" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-3.7383" y1="-0.2822" x2="-3.7383" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-3.7383" y1="0.2471" x2="-3.8089" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-3.8089" y1="0.3177" x2="-3.8089" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-3.8089" y1="0.5294" x2="-3.5619" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-3.5619" y1="0.5294" x2="-3.5619" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-3.5619" y1="1.6585" x2="-3.4207" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-3.4207" y1="1.6585" x2="-3.4207" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-3.4207" y1="0.5294" x2="-3.209" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-3.209" y1="0.5294" x2="-3.209" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-3.209" y1="0.3177" x2="-3.2796" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-3.2796" y1="0.2471" x2="-3.2796" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-2.7496" y1="-0.2822" x2="-2.7496" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-2.7496" y1="0.2471" x2="-2.8202" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-2.8202" y1="0.3177" x2="-2.8202" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-2.8202" y1="0.5294" x2="-2.5732" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-2.5732" y1="0.5294" x2="-2.5732" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-2.5732" y1="1.6585" x2="-2.432" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-2.432" y1="1.6585" x2="-2.432" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-2.432" y1="0.5294" x2="-2.2203" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-2.2203" y1="0.5294" x2="-2.2203" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-2.2203" y1="0.3177" x2="-2.2909" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-2.2909" y1="0.2471" x2="-2.2909" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-1.7616" y1="-0.2822" x2="-1.7616" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-1.7616" y1="0.2471" x2="-1.8322" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-1.8322" y1="0.3177" x2="-1.8322" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-1.8322" y1="0.5294" x2="-1.5852" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-1.5852" y1="0.5294" x2="-1.5852" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-1.5852" y1="1.6585" x2="-1.444" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-1.444" y1="1.6585" x2="-1.444" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-1.444" y1="0.5294" x2="-1.2323" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-1.2323" y1="0.5294" x2="-1.2323" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-1.2323" y1="0.3177" x2="-1.3029" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-1.3029" y1="0.2471" x2="-1.3029" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="-0.7383" y1="-0.2822" x2="-0.7383" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="-0.7383" y1="0.2471" x2="-0.8089" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-0.8089" y1="0.3177" x2="-0.8089" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-0.8089" y1="0.5294" x2="-0.5619" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-0.5619" y1="0.5294" x2="-0.5619" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-0.5619" y1="1.6585" x2="-0.4207" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="-0.4207" y1="1.6585" x2="-0.4207" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-0.4207" y1="0.5294" x2="-0.209" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="-0.209" y1="0.5294" x2="-0.209" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="-0.209" y1="0.3177" x2="-0.2796" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="-0.2796" y1="0.2471" x2="-0.2796" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="0.2504" y1="-0.2822" x2="0.2504" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="0.2504" y1="0.2471" x2="0.1798" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="0.1798" y1="0.3177" x2="0.1798" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="0.1798" y1="0.5294" x2="0.4268" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="0.4268" y1="0.5294" x2="0.4268" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="0.4268" y1="1.6585" x2="0.568" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="0.568" y1="1.6585" x2="0.568" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="0.568" y1="0.5294" x2="0.7797" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="0.7797" y1="0.5294" x2="0.7797" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="0.7797" y1="0.3177" x2="0.7091" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="0.7091" y1="0.2471" x2="0.7091" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="1.2384" y1="-0.2822" x2="1.2384" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="1.2384" y1="0.2471" x2="1.1678" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="1.1678" y1="0.3177" x2="1.1678" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="1.1678" y1="0.5294" x2="1.4148" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="1.4148" y1="0.5294" x2="1.4148" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="1.4148" y1="1.6585" x2="1.556" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="1.556" y1="1.6585" x2="1.556" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="1.556" y1="0.5294" x2="1.7677" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="1.7677" y1="0.5294" x2="1.7677" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="1.7677" y1="0.3177" x2="1.6971" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="1.6971" y1="0.2471" x2="1.6971" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="2.2617" y1="-0.2822" x2="2.2617" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="2.2617" y1="0.2471" x2="2.1911" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="2.1911" y1="0.3177" x2="2.1911" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="2.1911" y1="0.5294" x2="2.4381" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="2.4381" y1="0.5294" x2="2.4381" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="2.4381" y1="1.6585" x2="2.5793" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="2.5793" y1="1.6585" x2="2.5793" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="2.5793" y1="0.5294" x2="2.791" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="2.791" y1="0.5294" x2="2.791" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="2.791" y1="0.3177" x2="2.7204" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="2.7204" y1="0.2471" x2="2.7204" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="3.2504" y1="-0.2822" x2="3.2504" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="3.2504" y1="0.2471" x2="3.1798" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="3.1798" y1="0.3177" x2="3.1798" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="3.1798" y1="0.5294" x2="3.4268" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="3.4268" y1="0.5294" x2="3.4268" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="3.4268" y1="1.6585" x2="3.568" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="3.568" y1="1.6585" x2="3.568" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="3.568" y1="0.5294" x2="3.7797" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="3.7797" y1="0.5294" x2="3.7797" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="3.7797" y1="0.3177" x2="3.7091" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="3.7091" y1="0.2471" x2="3.7091" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="4.2384" y1="-0.2822" x2="4.2384" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="4.2384" y1="0.2471" x2="4.1678" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="4.1678" y1="0.3177" x2="4.1678" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="4.1678" y1="0.5294" x2="4.4148" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="4.4148" y1="0.5294" x2="4.4148" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="4.4148" y1="1.6585" x2="4.556" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="4.556" y1="1.6585" x2="4.556" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="4.556" y1="0.5294" x2="4.7677" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="4.7677" y1="0.5294" x2="4.7677" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="4.7677" y1="0.3177" x2="4.6971" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="4.6971" y1="0.2471" x2="4.6971" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="5.2617" y1="-0.2822" x2="5.2617" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="5.2617" y1="0.2471" x2="5.1911" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="5.1911" y1="0.3177" x2="5.1911" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="5.1911" y1="0.5294" x2="5.4381" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="5.4381" y1="0.5294" x2="5.4381" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="5.4381" y1="1.6585" x2="5.5793" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="5.5793" y1="1.6585" x2="5.5793" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="5.5793" y1="0.5294" x2="5.791" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="5.791" y1="0.5294" x2="5.791" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="5.791" y1="0.3177" x2="5.7204" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="5.7204" y1="0.2471" x2="5.7204" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="6.2504" y1="-0.2822" x2="6.2504" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="6.2504" y1="0.2471" x2="6.1798" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="6.1798" y1="0.3177" x2="6.1798" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="6.1798" y1="0.5294" x2="6.4268" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="6.4268" y1="0.5294" x2="6.4268" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="6.4268" y1="1.6585" x2="6.568" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="6.568" y1="1.6585" x2="6.568" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="6.568" y1="0.5294" x2="6.7797" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="6.7797" y1="0.5294" x2="6.7797" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="6.7797" y1="0.3177" x2="6.7091" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="6.7091" y1="0.2471" x2="6.7091" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="7.2384" y1="-0.2822" x2="7.2384" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="7.2384" y1="0.2471" x2="7.1678" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="7.1678" y1="0.3177" x2="7.1678" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="7.1678" y1="0.5294" x2="7.4148" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="7.4148" y1="0.5294" x2="7.4148" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="7.4148" y1="1.6585" x2="7.556" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="7.556" y1="1.6585" x2="7.556" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="7.556" y1="0.5294" x2="7.7677" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="7.7677" y1="0.5294" x2="7.7677" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="7.7677" y1="0.3177" x2="7.6971" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="7.6971" y1="0.2471" x2="7.6971" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="8.2617" y1="-0.2822" x2="8.2617" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="8.2617" y1="0.2471" x2="8.1911" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="8.1911" y1="0.3177" x2="8.1911" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="8.1911" y1="0.5294" x2="8.4381" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="8.4381" y1="0.5294" x2="8.4381" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="8.4381" y1="1.6585" x2="8.5793" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="8.5793" y1="1.6585" x2="8.5793" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="8.5793" y1="0.5294" x2="8.791" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="8.791" y1="0.5294" x2="8.791" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="8.791" y1="0.3177" x2="8.7204" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="8.7204" y1="0.2471" x2="8.7204" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="9.2504" y1="-0.2822" x2="9.2504" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="9.2504" y1="0.2471" x2="9.1798" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="9.1798" y1="0.3177" x2="9.1798" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="9.1798" y1="0.5294" x2="9.4268" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="9.4268" y1="0.5294" x2="9.4268" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="9.4268" y1="1.6585" x2="9.568" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="9.568" y1="1.6585" x2="9.568" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="9.568" y1="0.5294" x2="9.7797" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="9.7797" y1="0.5294" x2="9.7797" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="9.7797" y1="0.3177" x2="9.7091" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="9.7091" y1="0.2471" x2="9.7091" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="10.2384" y1="-0.2822" x2="10.2384" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="10.2384" y1="0.2471" x2="10.1678" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="10.1678" y1="0.3177" x2="10.1678" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="10.1678" y1="0.5294" x2="10.4148" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="10.4148" y1="0.5294" x2="10.4148" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="10.4148" y1="1.6585" x2="10.556" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="10.556" y1="1.6585" x2="10.556" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="10.556" y1="0.5294" x2="10.7677" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="10.7677" y1="0.5294" x2="10.7677" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="10.7677" y1="0.3177" x2="10.6971" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="10.6971" y1="0.2471" x2="10.6971" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="11.2617" y1="-0.2822" x2="11.2617" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="11.2617" y1="0.2471" x2="11.1911" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="11.1911" y1="0.3177" x2="11.1911" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="11.1911" y1="0.5294" x2="11.4381" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="11.4381" y1="0.5294" x2="11.4381" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="11.4381" y1="1.6585" x2="11.5793" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="11.5793" y1="1.6585" x2="11.5793" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="11.5793" y1="0.5294" x2="11.791" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="11.791" y1="0.5294" x2="11.791" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="11.791" y1="0.3177" x2="11.7204" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="11.7204" y1="0.2471" x2="11.7204" y2="-0.2822" width="0.1016" layer="21"/>
<wire x1="12.2617" y1="-0.2822" x2="12.2617" y2="0.2471" width="0.1016" layer="21"/>
<wire x1="12.2617" y1="0.2471" x2="12.1911" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="12.1911" y1="0.3177" x2="12.1911" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="12.1911" y1="0.5294" x2="12.4381" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="12.4381" y1="0.5294" x2="12.4381" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="12.4381" y1="1.6585" x2="12.5793" y2="1.6585" width="0.1016" layer="51"/>
<wire x1="12.5793" y1="1.6585" x2="12.5793" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="12.5793" y1="0.5294" x2="12.791" y2="0.5294" width="0.1016" layer="51"/>
<wire x1="12.791" y1="0.5294" x2="12.791" y2="0.3177" width="0.1016" layer="51"/>
<wire x1="12.791" y1="0.3177" x2="12.7204" y2="0.2471" width="0.1016" layer="51"/>
<wire x1="12.7204" y1="0.2471" x2="12.7204" y2="-0.2822" width="0.1016" layer="21"/>
<smd name="1" x="-15.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="2" x="-14.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="3" x="-13.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="4" x="-12.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="5" x="-11.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="6" x="-10.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="7" x="-9.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="8" x="-8.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="9" x="-7.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="10" x="-6.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="11" x="-5.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="12" x="-4.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="13" x="-3.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="14" x="-2.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="15" x="-1.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="16" x="-0.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="17" x="0.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="18" x="1.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="19" x="2.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="20" x="3.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="21" x="4.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="22" x="5.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="23" x="6.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="24" x="7.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="25" x="8.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="26" x="9.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="27" x="10.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="28" x="11.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="29" x="12.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="30" x="13.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="31" x="14.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="32" x="15.5" y="1.35" dx="0.6" dy="1.7" layer="1"/>
<smd name="M1" x="-19.725" y="-2.25" dx="1.2" dy="1.5" layer="1"/>
<smd name="M2" x="19.725" y="-2.25" dx="1.2" dy="1.5" layer="1"/>
<text x="-14.5" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="6" y="-3.04" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-20" y1="-2.85" x2="-18.65" y2="-1.65" layer="51"/>
<rectangle x1="18.65" y1="-2.85" x2="20" y2="-1.65" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FV">
<wire x1="0.889" y1="0.889" x2="0.889" y2="-0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<text x="1.27" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="F" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="F">
<wire x1="0.889" y1="0.889" x2="0.889" y2="-0.889" width="0.254" layer="94" curve="180" cap="flat"/>
<text x="1.27" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<pin name="F" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FI-XB30SS" prefix="X">
<description>&lt;b&gt;Connector FI-XB30SSL-HF15&lt;/b&gt;&lt;p&gt;
Source: http://jae-connector.com/en/pdf/SJ038317.pdf</description>
<gates>
<gate name="-1" symbol="FV" x="-2.54" y="30.48" addlevel="always"/>
<gate name="-2" symbol="F" x="-2.54" y="27.94" addlevel="always"/>
<gate name="-3" symbol="F" x="-2.54" y="25.4" addlevel="always"/>
<gate name="-4" symbol="F" x="-2.54" y="22.86" addlevel="always"/>
<gate name="-5" symbol="F" x="-2.54" y="20.32" addlevel="always"/>
<gate name="-6" symbol="F" x="-2.54" y="17.78" addlevel="always"/>
<gate name="-7" symbol="F" x="-2.54" y="15.24" addlevel="always"/>
<gate name="-8" symbol="F" x="-2.54" y="12.7" addlevel="always"/>
<gate name="-9" symbol="F" x="-2.54" y="10.16" addlevel="always"/>
<gate name="-10" symbol="F" x="-2.54" y="7.62" addlevel="always"/>
<gate name="-11" symbol="F" x="-2.54" y="5.08" addlevel="always"/>
<gate name="-12" symbol="F" x="-2.54" y="2.54" addlevel="always"/>
<gate name="-13" symbol="F" x="-2.54" y="0" addlevel="always"/>
<gate name="-14" symbol="F" x="-2.54" y="-2.54" addlevel="always"/>
<gate name="-15" symbol="F" x="-2.54" y="-5.08" addlevel="always"/>
<gate name="-16" symbol="F" x="-2.54" y="-7.62" addlevel="always"/>
<gate name="-17" symbol="F" x="-2.54" y="-10.16" addlevel="always"/>
<gate name="-18" symbol="F" x="-2.54" y="-12.7" addlevel="always"/>
<gate name="-19" symbol="F" x="-2.54" y="-15.24" addlevel="always"/>
<gate name="-20" symbol="F" x="-2.54" y="-17.78" addlevel="always"/>
<gate name="-21" symbol="F" x="-2.54" y="-20.32" addlevel="always"/>
<gate name="-22" symbol="F" x="-2.54" y="-22.86" addlevel="always"/>
<gate name="-23" symbol="F" x="-2.54" y="-25.4" addlevel="always"/>
<gate name="-24" symbol="F" x="-2.54" y="-27.94" addlevel="always"/>
<gate name="-25" symbol="F" x="-2.54" y="-30.48" addlevel="always"/>
<gate name="-26" symbol="F" x="-2.54" y="-33.02" addlevel="always"/>
<gate name="-27" symbol="F" x="-2.54" y="-35.56" addlevel="always"/>
<gate name="-28" symbol="F" x="-2.54" y="-38.1" addlevel="always"/>
<gate name="-29" symbol="F" x="-2.54" y="-40.64" addlevel="always"/>
<gate name="-30" symbol="F" x="-2.54" y="-43.18" addlevel="always"/>
<gate name="-31" symbol="F" x="-2.54" y="-45.72" addlevel="always"/>
<gate name="-32" symbol="F" x="-2.54" y="-48.26" addlevel="always"/>
</gates>
<devices>
<device name="" package="FI-XB30SSL-15">
<connects>
<connect gate="-1" pin="F" pad="1"/>
<connect gate="-10" pin="F" pad="10"/>
<connect gate="-11" pin="F" pad="11"/>
<connect gate="-12" pin="F" pad="12"/>
<connect gate="-13" pin="F" pad="13"/>
<connect gate="-14" pin="F" pad="14"/>
<connect gate="-15" pin="F" pad="15"/>
<connect gate="-16" pin="F" pad="16"/>
<connect gate="-17" pin="F" pad="17"/>
<connect gate="-18" pin="F" pad="18"/>
<connect gate="-19" pin="F" pad="19"/>
<connect gate="-2" pin="F" pad="2"/>
<connect gate="-20" pin="F" pad="20"/>
<connect gate="-21" pin="F" pad="21"/>
<connect gate="-22" pin="F" pad="22"/>
<connect gate="-23" pin="F" pad="23"/>
<connect gate="-24" pin="F" pad="24"/>
<connect gate="-25" pin="F" pad="25"/>
<connect gate="-26" pin="F" pad="26"/>
<connect gate="-27" pin="F" pad="27"/>
<connect gate="-28" pin="F" pad="28"/>
<connect gate="-29" pin="F" pad="29"/>
<connect gate="-3" pin="F" pad="3"/>
<connect gate="-30" pin="F" pad="30"/>
<connect gate="-31" pin="F" pad="31"/>
<connect gate="-32" pin="F" pad="32"/>
<connect gate="-4" pin="F" pad="4"/>
<connect gate="-5" pin="F" pad="5"/>
<connect gate="-6" pin="F" pad="6"/>
<connect gate="-7" pin="F" pad="7"/>
<connect gate="-8" pin="F" pad="8"/>
<connect gate="-9" pin="F" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames_Custom" deviceset="A4L-LOC" device="" value="7036-501A"/>
<part name="X1" library="con-jae" deviceset="FI-XB30SS" device="" value="FI-S25S (JAE)"/>
<part name="X2" library="con-jae" deviceset="FI-XB30SS" device="" value="DF14-20S-1.25C (HIROSE)"/>
<part name="X3" library="con-jae" deviceset="FI-XB30SS" device="" value="DF14-6S-1.25C (HIROSE)"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="58.42" y1="132.08" x2="93.98" y2="132.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="93.98" y1="132.08" x2="93.98" y2="58.42" width="0.1524" layer="97" style="shortdash"/>
<wire x1="93.98" y1="58.42" x2="58.42" y2="58.42" width="0.1524" layer="97" style="shortdash"/>
<wire x1="58.42" y1="58.42" x2="58.42" y2="129.54" width="0.1524" layer="97" style="shortdash"/>
<wire x1="58.42" y1="129.54" x2="58.42" y2="132.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="132.08" x2="208.28" y2="132.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="208.28" y1="132.08" x2="208.28" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="208.28" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="97" style="shortdash"/>
<wire x1="162.56" y1="71.12" x2="162.56" y2="132.08" width="0.1524" layer="97" style="shortdash"/>
<wire x1="81.28" y1="50.8" x2="175.26" y2="50.8" width="0.1524" layer="97"/>
<wire x1="175.26" y1="50.8" x2="172.72" y2="53.34" width="0.1524" layer="97"/>
<wire x1="175.26" y1="50.8" x2="172.72" y2="48.26" width="0.1524" layer="97"/>
<wire x1="81.28" y1="50.8" x2="83.82" y2="53.34" width="0.1524" layer="97"/>
<wire x1="81.28" y1="50.8" x2="83.82" y2="48.26" width="0.1524" layer="97"/>
<text x="119.38" y="48.26" size="1.778" layer="97">LENGTH see 4)</text>
<text x="162.56" y="66.04" size="1.778" layer="97">crimp contacts: FI-C3-A1-15000</text>
<text x="58.42" y="55.88" size="1.778" layer="97">crimp contacts: FI-C3-A1-15000</text>
<text x="12.7" y="38.1" size="2.54" layer="97"></text>
<text x="12.7" y="33.02" size="2.54" layer="97"></text>
<text x="12.7" y="27.94" size="2.54" layer="97"></text>
<text x="12.7" y="17.78" size="2.54" layer="97">1) COLORS ARE DEFINED, SEE DRAWING
2) UNLOADED CONTACTS ARE NOT PLACED IN CONNECTOR
3) TOTAL 18 WIRES, 36 CRIMP CONTACTS
4) LENGTH 100MM +- 10MM
5) TOTAL 4 TWISTED PAIRS: (5,6) (8,9) (11,12) (14,15)
6) LENGTH 100MM +- 10MM</text>
<text x="68.58" y="134.62" size="5.08" layer="95" ratio="15">POS1</text>
<text x="170.18" y="134.62" size="5.08" layer="95" ratio="15">POS2</text>
<text x="106.68" y="167.64" size="5.08" layer="95" ratio="15">POS3</text>
<wire x1="134.62" y1="165.1" x2="96.52" y2="165.1" width="0.1524" layer="97" style="shortdash"/>
<wire x1="96.52" y1="165.1" x2="96.52" y2="149.86" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="165.1" x2="134.62" y2="149.86" width="0.1524" layer="97" style="shortdash"/>
<wire x1="134.62" y1="149.86" x2="96.52" y2="149.86" width="0.1524" layer="97" style="shortdash"/>
<text x="157.48" y="124.46" size="1.27" layer="94" ratio="15">RED</text>
<text x="157.48" y="121.92" size="1.27" layer="94" ratio="15">RED</text>
<text x="157.48" y="116.84" size="1.27" layer="95" ratio="15">BLACK</text>
<text x="157.48" y="119.38" size="1.27" layer="95" ratio="15">BLACK</text>
<text x="137.16" y="109.22" size="1.016" layer="95">BLACK</text>
<text x="137.16" y="101.6" size="1.016" layer="95">BLACK</text>
<text x="137.16" y="93.98" size="1.016" layer="95">BLACK</text>
<text x="157.48" y="114.3" size="1.27" layer="95" ratio="15">WHITE</text>
<text x="157.48" y="111.76" size="1.27" layer="95" ratio="15">BLUE</text>
<text x="157.48" y="104.14" size="1.27" layer="95" ratio="15">BLUE</text>
<text x="157.48" y="104.14" size="1.27" layer="95" ratio="15">BLUE</text>
<text x="157.48" y="96.52" size="1.27" layer="95" ratio="15">BLUE</text>
<text x="157.48" y="88.9" size="1.27" layer="95" ratio="15">BLUE</text>
<text x="157.48" y="106.68" size="1.27" layer="95" ratio="15">WHITE</text>
<text x="157.48" y="99.06" size="1.27" layer="95" ratio="15">WHITE</text>
<text x="157.48" y="91.44" size="1.27" layer="95" ratio="15">WHITE</text>
<text x="96.52" y="119.38" size="1.27" layer="95" ratio="15">BLACK</text>
<text x="96.52" y="66.04" size="1.27" layer="95" ratio="15">WHITE</text>
<text x="96.52" y="63.5" size="1.27" layer="95" ratio="15">BLUE</text>
<wire x1="96.52" y1="121.92" x2="104.14" y2="121.92" width="0.1524" layer="98" style="shortdash"/>
<wire x1="104.14" y1="121.92" x2="104.14" y2="60.96" width="0.1524" layer="98" style="shortdash"/>
<wire x1="104.14" y1="60.96" x2="96.52" y2="60.96" width="0.1524" layer="98" style="shortdash"/>
<wire x1="96.52" y1="60.96" x2="96.52" y2="121.92" width="0.1524" layer="98" style="shortdash"/>
<wire x1="154.94" y1="127" x2="154.94" y2="86.36" width="0.1524" layer="98" style="shortdash"/>
<wire x1="154.94" y1="86.36" x2="165.1" y2="86.36" width="0.1524" layer="98" style="shortdash"/>
<wire x1="165.1" y1="86.36" x2="165.1" y2="127" width="0.1524" layer="98" style="shortdash"/>
<wire x1="165.1" y1="127" x2="154.94" y2="127" width="0.1524" layer="98" style="shortdash"/>
<text x="127" y="134.62" size="1.27" layer="97" ratio="15">CABLE COLORS</text>
<wire x1="137.16" y1="134.62" x2="154.94" y2="127" width="0.1524" layer="97" style="shortdash"/>
<wire x1="137.16" y1="134.62" x2="104.14" y2="121.92" width="0.1524" layer="97" style="shortdash"/>
<wire x1="45.72" y1="157.48" x2="45.72" y2="58.42" width="0.1524" layer="97"/>
<wire x1="45.72" y1="58.42" x2="48.26" y2="60.96" width="0.1524" layer="97"/>
<wire x1="45.72" y1="58.42" x2="43.18" y2="60.96" width="0.1524" layer="97"/>
<wire x1="45.72" y1="157.48" x2="48.26" y2="154.94" width="0.1524" layer="97"/>
<wire x1="45.72" y1="157.48" x2="43.18" y2="154.94" width="0.1524" layer="97"/>
<text x="43.18" y="109.22" size="1.778" layer="97" rot="R270">LENGTH see 6)</text>
<wire x1="45.72" y1="157.48" x2="93.98" y2="157.48" width="0.1524" layer="97" style="shortdash"/>
<wire x1="121.92" y1="144.78" x2="175.26" y2="144.78" width="0.1524" layer="97"/>
<wire x1="175.26" y1="144.78" x2="172.72" y2="147.32" width="0.1524" layer="97"/>
<wire x1="175.26" y1="144.78" x2="172.72" y2="142.24" width="0.1524" layer="97"/>
<wire x1="121.92" y1="144.78" x2="124.46" y2="147.32" width="0.1524" layer="97"/>
<wire x1="121.92" y1="144.78" x2="124.46" y2="142.24" width="0.1524" layer="97"/>
<text x="139.7" y="142.24" size="1.778" layer="97">LENGTH see 4)</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="X1" gate="-1" x="81.28" y="124.46" smashed="yes" rot="MR0">
<attribute name="NAME" x="80.01" y="123.698" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="83.82" y="128.397" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="X1" gate="-2" x="81.28" y="121.92" rot="MR0"/>
<instance part="X1" gate="-3" x="81.28" y="119.38" rot="MR0"/>
<instance part="X1" gate="-4" x="81.28" y="116.84" rot="MR0"/>
<instance part="X1" gate="-5" x="81.28" y="114.3" rot="MR0"/>
<instance part="X1" gate="-6" x="81.28" y="111.76" rot="MR0"/>
<instance part="X1" gate="-7" x="81.28" y="109.22" rot="MR0"/>
<instance part="X1" gate="-8" x="81.28" y="106.68" rot="MR0"/>
<instance part="X1" gate="-9" x="81.28" y="104.14" rot="MR0"/>
<instance part="X1" gate="-10" x="81.28" y="101.6" rot="MR0"/>
<instance part="X1" gate="-11" x="81.28" y="99.06" rot="MR0"/>
<instance part="X1" gate="-12" x="81.28" y="96.52" rot="MR0"/>
<instance part="X1" gate="-13" x="81.28" y="93.98" rot="MR0"/>
<instance part="X1" gate="-14" x="81.28" y="91.44" rot="MR0"/>
<instance part="X1" gate="-15" x="81.28" y="88.9" rot="MR0"/>
<instance part="X1" gate="-16" x="81.28" y="86.36" rot="MR0"/>
<instance part="X1" gate="-17" x="81.28" y="83.82" rot="MR0"/>
<instance part="X1" gate="-18" x="81.28" y="81.28" rot="MR0"/>
<instance part="X1" gate="-19" x="81.28" y="78.74" rot="MR0"/>
<instance part="X1" gate="-20" x="81.28" y="76.2" rot="MR0"/>
<instance part="X1" gate="-21" x="81.28" y="73.66" rot="MR0"/>
<instance part="X1" gate="-22" x="81.28" y="71.12" rot="MR0"/>
<instance part="X1" gate="-23" x="81.28" y="68.58" rot="MR0"/>
<instance part="X1" gate="-24" x="81.28" y="66.04" rot="MR0"/>
<instance part="X1" gate="-25" x="81.28" y="63.5" rot="MR0"/>
<instance part="X2" gate="-1" x="175.26" y="124.46" smashed="yes">
<attribute name="NAME" x="176.53" y="123.698" size="1.778" layer="95"/>
<attribute name="VALUE" x="167.64" y="128.397" size="1.778" layer="96"/>
</instance>
<instance part="X2" gate="-2" x="175.26" y="121.92" smashed="yes">
<attribute name="NAME" x="176.53" y="121.158" size="1.778" layer="95"/>
</instance>
<instance part="X2" gate="-3" x="175.26" y="119.38"/>
<instance part="X2" gate="-4" x="175.26" y="116.84"/>
<instance part="X2" gate="-5" x="175.26" y="114.3"/>
<instance part="X2" gate="-6" x="175.26" y="111.76"/>
<instance part="X2" gate="-7" x="175.26" y="109.22"/>
<instance part="X2" gate="-8" x="175.26" y="106.68"/>
<instance part="X2" gate="-9" x="175.26" y="104.14"/>
<instance part="X2" gate="-10" x="175.26" y="101.6"/>
<instance part="X2" gate="-11" x="175.26" y="99.06"/>
<instance part="X2" gate="-12" x="175.26" y="96.52"/>
<instance part="X2" gate="-13" x="175.26" y="93.98"/>
<instance part="X2" gate="-14" x="175.26" y="91.44"/>
<instance part="X2" gate="-15" x="175.26" y="88.9"/>
<instance part="X2" gate="-16" x="175.26" y="86.36"/>
<instance part="X2" gate="-17" x="175.26" y="83.82"/>
<instance part="X2" gate="-18" x="175.26" y="81.28"/>
<instance part="X2" gate="-19" x="175.26" y="78.74"/>
<instance part="X2" gate="-20" x="175.26" y="76.2"/>
<instance part="X3" gate="-1" x="119.38" y="152.4" smashed="yes" rot="MR90">
<attribute name="NAME" x="118.618" y="153.67" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="99.06" y="163.703" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="X3" gate="-2" x="116.84" y="152.4" rot="MR90"/>
<instance part="X3" gate="-3" x="114.3" y="152.4" rot="MR90"/>
<instance part="X3" gate="-4" x="111.76" y="152.4" rot="MR90"/>
<instance part="X3" gate="-5" x="109.22" y="152.4" rot="MR90"/>
<instance part="X3" gate="-6" x="106.68" y="152.4" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="83.82" y1="116.84" x2="172.72" y2="116.84" width="0.1524" layer="91"/>
<label x="83.82" y="116.84" size="1.016" layer="95"/>
<pinref part="X2" gate="-4" pin="F"/>
<pinref part="X1" gate="-4" pin="F"/>
<label x="167.64" y="116.84" size="1.016" layer="95"/>
</segment>
<segment>
<wire x1="83.82" y1="109.22" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<label x="83.82" y="109.22" size="1.016" layer="95"/>
<pinref part="X2" gate="-7" pin="F"/>
<pinref part="X1" gate="-7" pin="F"/>
<label x="167.64" y="109.22" size="1.016" layer="95"/>
</segment>
<segment>
<wire x1="83.82" y1="101.6" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<label x="83.82" y="101.6" size="1.016" layer="95"/>
<pinref part="X2" gate="-10" pin="F"/>
<pinref part="X1" gate="-10" pin="F"/>
<label x="167.64" y="101.6" size="1.016" layer="95"/>
</segment>
<segment>
<wire x1="83.82" y1="93.98" x2="172.72" y2="93.98" width="0.1524" layer="91"/>
<label x="83.82" y="93.98" size="1.016" layer="95"/>
<pinref part="X2" gate="-13" pin="F"/>
<pinref part="X1" gate="-13" pin="F"/>
<label x="167.64" y="93.98" size="1.016" layer="95"/>
</segment>
<segment>
<pinref part="X2" gate="-3" pin="F"/>
<wire x1="172.72" y1="119.38" x2="114.3" y2="119.38" width="0.1524" layer="91"/>
<wire x1="114.3" y1="119.38" x2="114.3" y2="149.86" width="0.1524" layer="91"/>
<label x="167.64" y="119.38" size="1.016" layer="95"/>
<pinref part="X3" gate="-3" pin="F"/>
</segment>
<segment>
<pinref part="X1" gate="-3" pin="F"/>
<pinref part="X3" gate="-6" pin="F"/>
<wire x1="83.82" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="106.68" y1="119.38" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<label x="83.82" y="119.38" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_CLK-" class="0">
<segment>
<wire x1="83.82" y1="91.44" x2="93.98" y2="91.44" width="0.1524" layer="91"/>
<wire x1="93.98" y1="91.44" x2="96.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="96.52" y1="88.9" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<wire x1="99.06" y1="88.9" x2="101.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="101.6" y1="91.44" x2="104.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="104.14" y1="91.44" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<wire x1="106.68" y1="88.9" x2="109.22" y2="88.9" width="0.1524" layer="91"/>
<wire x1="109.22" y1="88.9" x2="111.76" y2="91.44" width="0.1524" layer="91"/>
<wire x1="111.76" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<wire x1="114.3" y1="91.44" x2="116.84" y2="88.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="88.9" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="119.38" y1="88.9" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<wire x1="121.92" y1="91.44" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<wire x1="124.46" y1="91.44" x2="127" y2="88.9" width="0.1524" layer="91"/>
<wire x1="127" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="88.9" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
<wire x1="132.08" y1="91.44" x2="134.62" y2="91.44" width="0.1524" layer="91"/>
<wire x1="134.62" y1="91.44" x2="137.16" y2="88.9" width="0.1524" layer="91"/>
<wire x1="137.16" y1="88.9" x2="139.7" y2="88.9" width="0.1524" layer="91"/>
<wire x1="139.7" y1="88.9" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<wire x1="142.24" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<wire x1="144.78" y1="91.44" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="147.32" y1="88.9" x2="149.86" y2="88.9" width="0.1524" layer="91"/>
<wire x1="149.86" y1="88.9" x2="152.4" y2="91.44" width="0.1524" layer="91"/>
<wire x1="152.4" y1="91.44" x2="154.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="154.94" y1="88.9" x2="157.48" y2="91.44" width="0.1524" layer="91"/>
<wire x1="157.48" y1="91.44" x2="172.72" y2="91.44" width="0.1524" layer="91"/>
<label x="83.82" y="91.44" size="1.016" layer="95"/>
<label x="165.1" y="91.44" size="1.016" layer="95"/>
<pinref part="X2" gate="-14" pin="F"/>
<pinref part="X1" gate="-14" pin="F"/>
</segment>
</net>
<net name="LVDS_CLK+" class="0">
<segment>
<wire x1="83.82" y1="88.9" x2="93.98" y2="88.9" width="0.1524" layer="91"/>
<wire x1="93.98" y1="88.9" x2="96.52" y2="91.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="91.44" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="101.6" y2="88.9" width="0.1524" layer="91"/>
<wire x1="101.6" y1="88.9" x2="104.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="104.14" y1="88.9" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<wire x1="106.68" y1="91.44" x2="109.22" y2="91.44" width="0.1524" layer="91"/>
<wire x1="109.22" y1="91.44" x2="111.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="111.76" y1="88.9" x2="114.3" y2="88.9" width="0.1524" layer="91"/>
<wire x1="114.3" y1="88.9" x2="116.84" y2="91.44" width="0.1524" layer="91"/>
<wire x1="116.84" y1="91.44" x2="119.38" y2="91.44" width="0.1524" layer="91"/>
<wire x1="119.38" y1="91.44" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="121.92" y1="88.9" x2="124.46" y2="88.9" width="0.1524" layer="91"/>
<wire x1="124.46" y1="88.9" x2="127" y2="91.44" width="0.1524" layer="91"/>
<wire x1="127" y1="91.44" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<wire x1="129.54" y1="91.44" x2="132.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="132.08" y1="88.9" x2="134.62" y2="88.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="88.9" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<wire x1="137.16" y1="91.44" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
<wire x1="139.7" y1="91.44" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="142.24" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<wire x1="144.78" y1="88.9" x2="147.32" y2="91.44" width="0.1524" layer="91"/>
<wire x1="147.32" y1="91.44" x2="149.86" y2="91.44" width="0.1524" layer="91"/>
<wire x1="149.86" y1="91.44" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
<wire x1="152.4" y1="88.9" x2="154.94" y2="91.44" width="0.1524" layer="91"/>
<wire x1="154.94" y1="91.44" x2="157.48" y2="88.9" width="0.1524" layer="91"/>
<wire x1="157.48" y1="88.9" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<wire x1="170.18" y1="88.9" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<label x="83.82" y="88.9" size="1.016" layer="95"/>
<label x="165.1" y="88.9" size="1.016" layer="95"/>
<pinref part="X2" gate="-15" pin="F"/>
<pinref part="X1" gate="-15" pin="F"/>
</segment>
</net>
<net name="LVDS_DATA2+" class="0">
<segment>
<wire x1="83.82" y1="96.52" x2="93.98" y2="96.52" width="0.1524" layer="91"/>
<wire x1="93.98" y1="96.52" x2="96.52" y2="99.06" width="0.1524" layer="91"/>
<wire x1="96.52" y1="99.06" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
<wire x1="99.06" y1="99.06" x2="101.6" y2="96.52" width="0.1524" layer="91"/>
<wire x1="101.6" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="96.52" x2="106.68" y2="99.06" width="0.1524" layer="91"/>
<wire x1="106.68" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<wire x1="109.22" y1="99.06" x2="111.76" y2="96.52" width="0.1524" layer="91"/>
<wire x1="111.76" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<wire x1="114.3" y1="96.52" x2="116.84" y2="99.06" width="0.1524" layer="91"/>
<wire x1="116.84" y1="99.06" x2="119.38" y2="99.06" width="0.1524" layer="91"/>
<wire x1="119.38" y1="99.06" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<wire x1="121.92" y1="96.52" x2="124.46" y2="96.52" width="0.1524" layer="91"/>
<wire x1="124.46" y1="96.52" x2="127" y2="99.06" width="0.1524" layer="91"/>
<wire x1="127" y1="99.06" x2="129.54" y2="99.06" width="0.1524" layer="91"/>
<wire x1="129.54" y1="99.06" x2="132.08" y2="96.52" width="0.1524" layer="91"/>
<wire x1="132.08" y1="96.52" x2="134.62" y2="96.52" width="0.1524" layer="91"/>
<wire x1="134.62" y1="96.52" x2="137.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="137.16" y1="99.06" x2="139.7" y2="99.06" width="0.1524" layer="91"/>
<wire x1="139.7" y1="99.06" x2="142.24" y2="96.52" width="0.1524" layer="91"/>
<wire x1="142.24" y1="96.52" x2="144.78" y2="96.52" width="0.1524" layer="91"/>
<wire x1="144.78" y1="96.52" x2="147.32" y2="99.06" width="0.1524" layer="91"/>
<wire x1="147.32" y1="99.06" x2="149.86" y2="99.06" width="0.1524" layer="91"/>
<wire x1="149.86" y1="99.06" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<wire x1="152.4" y1="96.52" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="154.94" y1="99.06" x2="157.48" y2="96.52" width="0.1524" layer="91"/>
<wire x1="157.48" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="170.18" y1="96.52" x2="172.72" y2="96.52" width="0.1524" layer="91"/>
<label x="83.82" y="96.52" size="1.016" layer="95"/>
<label x="165.1" y="96.52" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_DATA2-" class="0">
<segment>
<wire x1="83.82" y1="99.06" x2="93.98" y2="99.06" width="0.1524" layer="91"/>
<wire x1="93.98" y1="99.06" x2="96.52" y2="96.52" width="0.1524" layer="91"/>
<wire x1="96.52" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<wire x1="99.06" y1="96.52" x2="101.6" y2="99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="104.14" y1="99.06" x2="106.68" y2="96.52" width="0.1524" layer="91"/>
<wire x1="106.68" y1="96.52" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="111.76" y2="99.06" width="0.1524" layer="91"/>
<wire x1="111.76" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="114.3" y1="99.06" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<wire x1="116.84" y1="96.52" x2="119.38" y2="96.52" width="0.1524" layer="91"/>
<wire x1="119.38" y1="96.52" x2="121.92" y2="99.06" width="0.1524" layer="91"/>
<wire x1="121.92" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="124.46" y1="99.06" x2="127" y2="96.52" width="0.1524" layer="91"/>
<wire x1="127" y1="96.52" x2="129.54" y2="96.52" width="0.1524" layer="91"/>
<wire x1="129.54" y1="96.52" x2="132.08" y2="99.06" width="0.1524" layer="91"/>
<wire x1="132.08" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<wire x1="134.62" y1="99.06" x2="137.16" y2="96.52" width="0.1524" layer="91"/>
<wire x1="137.16" y1="96.52" x2="139.7" y2="96.52" width="0.1524" layer="91"/>
<wire x1="139.7" y1="96.52" x2="142.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="142.24" y1="99.06" x2="144.78" y2="99.06" width="0.1524" layer="91"/>
<wire x1="144.78" y1="99.06" x2="147.32" y2="96.52" width="0.1524" layer="91"/>
<wire x1="147.32" y1="96.52" x2="149.86" y2="96.52" width="0.1524" layer="91"/>
<wire x1="149.86" y1="96.52" x2="152.4" y2="99.06" width="0.1524" layer="91"/>
<wire x1="152.4" y1="99.06" x2="154.94" y2="96.52" width="0.1524" layer="91"/>
<wire x1="154.94" y1="96.52" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<wire x1="157.48" y1="99.06" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<label x="83.82" y="99.06" size="1.016" layer="95"/>
<label x="165.1" y="99.06" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_DATA1+" class="0">
<segment>
<wire x1="83.82" y1="104.14" x2="93.98" y2="104.14" width="0.1524" layer="91"/>
<wire x1="93.98" y1="104.14" x2="96.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="96.52" y1="106.68" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
<wire x1="99.06" y1="106.68" x2="101.6" y2="104.14" width="0.1524" layer="91"/>
<wire x1="101.6" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="104.14" y1="104.14" x2="106.68" y2="106.68" width="0.1524" layer="91"/>
<wire x1="106.68" y1="106.68" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
<wire x1="109.22" y1="106.68" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<wire x1="111.76" y1="104.14" x2="114.3" y2="104.14" width="0.1524" layer="91"/>
<wire x1="114.3" y1="104.14" x2="116.84" y2="106.68" width="0.1524" layer="91"/>
<wire x1="116.84" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<wire x1="119.38" y1="106.68" x2="121.92" y2="104.14" width="0.1524" layer="91"/>
<wire x1="121.92" y1="104.14" x2="124.46" y2="104.14" width="0.1524" layer="91"/>
<wire x1="124.46" y1="104.14" x2="127" y2="106.68" width="0.1524" layer="91"/>
<wire x1="127" y1="106.68" x2="129.54" y2="106.68" width="0.1524" layer="91"/>
<wire x1="129.54" y1="106.68" x2="132.08" y2="104.14" width="0.1524" layer="91"/>
<wire x1="132.08" y1="104.14" x2="134.62" y2="104.14" width="0.1524" layer="91"/>
<wire x1="134.62" y1="104.14" x2="137.16" y2="106.68" width="0.1524" layer="91"/>
<wire x1="137.16" y1="106.68" x2="139.7" y2="106.68" width="0.1524" layer="91"/>
<wire x1="139.7" y1="106.68" x2="142.24" y2="104.14" width="0.1524" layer="91"/>
<wire x1="142.24" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<wire x1="144.78" y1="104.14" x2="147.32" y2="106.68" width="0.1524" layer="91"/>
<wire x1="147.32" y1="106.68" x2="149.86" y2="106.68" width="0.1524" layer="91"/>
<wire x1="149.86" y1="106.68" x2="152.4" y2="104.14" width="0.1524" layer="91"/>
<wire x1="152.4" y1="104.14" x2="154.94" y2="106.68" width="0.1524" layer="91"/>
<wire x1="154.94" y1="106.68" x2="157.48" y2="104.14" width="0.1524" layer="91"/>
<wire x1="157.48" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<wire x1="170.18" y1="104.14" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<label x="83.82" y="104.14" size="1.016" layer="95"/>
<label x="165.1" y="104.14" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_DATA1-" class="0">
<segment>
<wire x1="83.82" y1="106.68" x2="93.98" y2="106.68" width="0.1524" layer="91"/>
<wire x1="93.98" y1="106.68" x2="96.52" y2="104.14" width="0.1524" layer="91"/>
<wire x1="96.52" y1="104.14" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
<wire x1="99.06" y1="104.14" x2="101.6" y2="106.68" width="0.1524" layer="91"/>
<wire x1="101.6" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="104.14" y1="106.68" x2="106.68" y2="104.14" width="0.1524" layer="91"/>
<wire x1="106.68" y1="104.14" x2="109.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="109.22" y1="104.14" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<wire x1="111.76" y1="106.68" x2="114.3" y2="106.68" width="0.1524" layer="91"/>
<wire x1="114.3" y1="106.68" x2="116.84" y2="104.14" width="0.1524" layer="91"/>
<wire x1="116.84" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<wire x1="119.38" y1="104.14" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="121.92" y1="106.68" x2="124.46" y2="106.68" width="0.1524" layer="91"/>
<wire x1="124.46" y1="106.68" x2="127" y2="104.14" width="0.1524" layer="91"/>
<wire x1="127" y1="104.14" x2="129.54" y2="104.14" width="0.1524" layer="91"/>
<wire x1="129.54" y1="104.14" x2="132.08" y2="106.68" width="0.1524" layer="91"/>
<wire x1="132.08" y1="106.68" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<wire x1="134.62" y1="106.68" x2="137.16" y2="104.14" width="0.1524" layer="91"/>
<wire x1="137.16" y1="104.14" x2="139.7" y2="104.14" width="0.1524" layer="91"/>
<wire x1="139.7" y1="104.14" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
<wire x1="142.24" y1="106.68" x2="144.78" y2="106.68" width="0.1524" layer="91"/>
<wire x1="144.78" y1="106.68" x2="147.32" y2="104.14" width="0.1524" layer="91"/>
<wire x1="147.32" y1="104.14" x2="149.86" y2="104.14" width="0.1524" layer="91"/>
<wire x1="149.86" y1="104.14" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<wire x1="152.4" y1="106.68" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<wire x1="154.94" y1="104.14" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<wire x1="157.48" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<label x="83.82" y="106.68" size="1.016" layer="95"/>
<label x="165.1" y="106.68" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_DATA0+" class="0">
<segment>
<wire x1="83.82" y1="111.76" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<wire x1="93.98" y1="111.76" x2="96.52" y2="114.3" width="0.1524" layer="91"/>
<wire x1="96.52" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<wire x1="99.06" y1="114.3" x2="101.6" y2="111.76" width="0.1524" layer="91"/>
<wire x1="101.6" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
<wire x1="104.14" y1="111.76" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<wire x1="106.68" y1="114.3" x2="109.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="109.22" y1="114.3" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<wire x1="111.76" y1="111.76" x2="114.3" y2="111.76" width="0.1524" layer="91"/>
<wire x1="114.3" y1="111.76" x2="116.84" y2="114.3" width="0.1524" layer="91"/>
<wire x1="116.84" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="119.38" y1="114.3" x2="121.92" y2="111.76" width="0.1524" layer="91"/>
<wire x1="121.92" y1="111.76" x2="124.46" y2="111.76" width="0.1524" layer="91"/>
<wire x1="124.46" y1="111.76" x2="127" y2="114.3" width="0.1524" layer="91"/>
<wire x1="127" y1="114.3" x2="129.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="129.54" y1="114.3" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
<wire x1="132.08" y1="111.76" x2="134.62" y2="111.76" width="0.1524" layer="91"/>
<wire x1="134.62" y1="111.76" x2="137.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="137.16" y1="114.3" x2="139.7" y2="114.3" width="0.1524" layer="91"/>
<wire x1="139.7" y1="114.3" x2="142.24" y2="111.76" width="0.1524" layer="91"/>
<wire x1="142.24" y1="111.76" x2="144.78" y2="111.76" width="0.1524" layer="91"/>
<wire x1="144.78" y1="111.76" x2="147.32" y2="114.3" width="0.1524" layer="91"/>
<wire x1="147.32" y1="114.3" x2="149.86" y2="114.3" width="0.1524" layer="91"/>
<wire x1="149.86" y1="114.3" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<wire x1="152.4" y1="111.76" x2="154.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="154.94" y1="114.3" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<wire x1="157.48" y1="111.76" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
<wire x1="170.18" y1="111.76" x2="172.72" y2="111.76" width="0.1524" layer="91"/>
<label x="83.82" y="111.76" size="1.016" layer="95"/>
<label x="165.1" y="111.76" size="1.016" layer="95"/>
</segment>
</net>
<net name="LVDS_DATA0-" class="0">
<segment>
<wire x1="83.82" y1="114.3" x2="93.98" y2="114.3" width="0.1524" layer="91"/>
<wire x1="93.98" y1="114.3" x2="96.52" y2="111.76" width="0.1524" layer="91"/>
<wire x1="96.52" y1="111.76" x2="99.06" y2="111.76" width="0.1524" layer="91"/>
<wire x1="99.06" y1="111.76" x2="101.6" y2="114.3" width="0.1524" layer="91"/>
<wire x1="101.6" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<wire x1="104.14" y1="114.3" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<wire x1="106.68" y1="111.76" x2="109.22" y2="111.76" width="0.1524" layer="91"/>
<wire x1="109.22" y1="111.76" x2="111.76" y2="114.3" width="0.1524" layer="91"/>
<wire x1="111.76" y1="114.3" x2="114.3" y2="114.3" width="0.1524" layer="91"/>
<wire x1="114.3" y1="114.3" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="116.84" y1="111.76" x2="119.38" y2="111.76" width="0.1524" layer="91"/>
<wire x1="119.38" y1="111.76" x2="121.92" y2="114.3" width="0.1524" layer="91"/>
<wire x1="121.92" y1="114.3" x2="124.46" y2="114.3" width="0.1524" layer="91"/>
<wire x1="124.46" y1="114.3" x2="127" y2="111.76" width="0.1524" layer="91"/>
<wire x1="127" y1="111.76" x2="129.54" y2="111.76" width="0.1524" layer="91"/>
<wire x1="129.54" y1="111.76" x2="132.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="132.08" y1="114.3" x2="134.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="134.62" y1="114.3" x2="137.16" y2="111.76" width="0.1524" layer="91"/>
<wire x1="137.16" y1="111.76" x2="139.7" y2="111.76" width="0.1524" layer="91"/>
<wire x1="139.7" y1="111.76" x2="142.24" y2="114.3" width="0.1524" layer="91"/>
<wire x1="142.24" y1="114.3" x2="144.78" y2="114.3" width="0.1524" layer="91"/>
<wire x1="144.78" y1="114.3" x2="147.32" y2="111.76" width="0.1524" layer="91"/>
<wire x1="147.32" y1="111.76" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
<wire x1="149.86" y1="111.76" x2="152.4" y2="114.3" width="0.1524" layer="91"/>
<wire x1="152.4" y1="114.3" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="154.94" y1="111.76" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<wire x1="157.48" y1="114.3" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<label x="83.82" y="114.3" size="1.016" layer="95"/>
<label x="165.1" y="114.3" size="1.016" layer="95"/>
</segment>
</net>
<net name="BL-PWM" class="0">
<segment>
<pinref part="X1" gate="-25" pin="F"/>
<wire x1="83.82" y1="63.5" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="63.5" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<label x="83.82" y="63.5" size="1.016" layer="95"/>
<pinref part="X3" gate="-4" pin="F"/>
</segment>
</net>
<net name="BL-ON" class="0">
<segment>
<pinref part="X1" gate="-24" pin="F"/>
<wire x1="83.82" y1="66.04" x2="109.22" y2="66.04" width="0.1524" layer="91"/>
<wire x1="109.22" y1="66.04" x2="109.22" y2="149.86" width="0.1524" layer="91"/>
<label x="83.82" y="66.04" size="1.016" layer="95"/>
<pinref part="X3" gate="-5" pin="F"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="X2" gate="-1" pin="F"/>
<wire x1="172.72" y1="124.46" x2="119.38" y2="124.46" width="0.1524" layer="91"/>
<wire x1="119.38" y1="124.46" x2="119.38" y2="149.86" width="0.1524" layer="91"/>
<label x="167.64" y="124.46" size="1.016" layer="95"/>
<pinref part="X3" gate="-1" pin="F"/>
</segment>
<segment>
<pinref part="X2" gate="-2" pin="F"/>
<wire x1="172.72" y1="121.92" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
<wire x1="116.84" y1="121.92" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<label x="167.64" y="121.92" size="1.016" layer="95"/>
<pinref part="X3" gate="-2" pin="F"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
